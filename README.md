# SchoolConsoleApp



This is a Java Spring Boot Application that demonstrates the basic setup and structure for a console application. The console menu uses implemented CRUD functionality for courses, groups, and students.

### Stack

- Java 17
- Maven
- Spring Boot 3.2.1
- Lombok
- Docker
- Flyway Migration
- PostgreSQL 15.5
- Hibernate
- JUnit 5
- Mockito
