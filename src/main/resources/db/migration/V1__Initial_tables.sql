CREATE TABLE courses(
    course_id BIGSERIAL NOT NULL,
    course_name CHARACTER VARYING (20) NOT NULL,
    course_description CHARACTER VARYING (120) NOT NULL,
    PRIMARY KEY (course_id)
);

CREATE TABLE groups(
    group_id BIGSERIAL NOT NULL,
    group_name CHARACTER VARYING (5) NOT NULL,
    PRIMARY KEY (group_id)
);

CREATE TABLE students(
    student_id BIGSERIAL NOT NULL,
    group_id BIGINT,
    first_name CHARACTER VARYING (30) NOT NULL,
    last_name CHARACTER VARYING (30) NOT NULL,
    PRIMARY KEY (student_id),
    FOREIGN KEY (group_id) REFERENCES groups (group_id) ON DELETE CASCADE ON UPDATE CASCADE
);
