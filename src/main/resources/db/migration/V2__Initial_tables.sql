DROP TABLE IF EXISTS courses, groups, students, courses_students CASCADE;

CREATE TABLE IF NOT EXISTS courses(
    course_id BIGSERIAL NOT NULL,
    course_name CHARACTER VARYING (20) NOT NULL,
    course_description CHARACTER VARYING (120) NOT NULL,
    PRIMARY KEY (course_id)
);

CREATE TABLE IF NOT EXISTS groups(
    group_id BIGSERIAL NOT NULL,
    group_name CHARACTER VARYING (5) NOT NULL,
    PRIMARY KEY (group_id)
);

CREATE TABLE IF NOT EXISTS students(
    student_id BIGSERIAL NOT NULL,
    group_id BIGINT,
    first_name CHARACTER VARYING (30) NOT NULL,
    last_name CHARACTER VARYING (30) NOT NULL,
    PRIMARY KEY (student_id),
    FOREIGN KEY (group_id) REFERENCES groups (group_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS courses_students(
    course_id BIGINT,
    student_id BIGINT,
    PRIMARY KEY (course_id, student_id),
    CONSTRAINT courses_students_fk_1 FOREIGN KEY (course_id) REFERENCES courses (course_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT courses_students_fk_2 FOREIGN KEY (student_id) REFERENCES students (student_id) ON DELETE CASCADE ON UPDATE CASCADE
);
