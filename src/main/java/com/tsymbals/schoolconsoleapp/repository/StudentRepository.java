package com.tsymbals.schoolconsoleapp.repository;

import com.tsymbals.schoolconsoleapp.entity.Student;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("SELECT student FROM Student student WHERE student.firstName = :firstName AND student.lastName = :lastName")
    Optional<Student> findByFirstNameAndLastName(String firstName, String lastName);
}
