package com.tsymbals.schoolconsoleapp.menuitems;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
public abstract class MenuItem {

    private int number;
    private String description;

    public abstract void doAction();

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        MenuItem menuItem = (MenuItem) object;
        return number == menuItem.number && Objects.equals(description, menuItem.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, description);
    }
}
