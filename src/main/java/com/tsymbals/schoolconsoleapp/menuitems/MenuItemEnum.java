package com.tsymbals.schoolconsoleapp.menuitems;

import lombok.Getter;

@Getter
public enum MenuItemEnum {

    SAVE_NEW_COURSE(1, "For saving new course"),
    DISPLAY_COURSES(2, "For displaying courses"),
    DISPLAY_COURSE_BY_ID(3, "For displaying course by id"),
    DISPLAY_COURSE_BY_NAME(4, "For displaying course by name"),
    UPDATE_COURSE_NAME(5, "For updating course name"),
    UPDATE_COURSE_DESCRIPTION(6, "For updating course description"),
    DELETE_COURSE_BY_ID(7, "For deleting course by id"),
    DELETE_COURSE_BY_NAME(8, "For deleting course by name"),
    SAVE_NEW_GROUP(9, "For saving new group"),
    DISPLAY_GROUPS(10, "For displaying groups"),
    DISPLAY_GROUP_BY_ID(11, "For displaying group by id"),
    DISPLAY_GROUP_BY_NAME(12, "For displaying group by name"),
    UPDATE_GROUP_NAME(13, "For updating group name"),
    DELETE_GROUP_BY_ID(14, "For deleting group by id"),
    DELETE_GROUP_BY_NAME(15, "For deleting group by name"),
    SAVE_NEW_STUDENT(16, "For saving new student"),
    DISPLAY_STUDENTS(17, "For displaying students"),
    DISPLAY_STUDENT_BY_ID(18, "For displaying student by id"),
    DISPLAY_STUDENT_BY_NAME(19, "For displaying student by first and last names"),
    UPDATE_STUDENT_LAST_NAME(20, "For updating student last name"),
    UPDATE_STUDENT_BY_ADD_GROUP(21, "For adding group to student"),
    UPDATE_STUDENT_BY_DELETE_GROUP(22, "For deleting group from student"),
    UPDATE_STUDENT_BY_ADD_COURSE(23, "For adding course to student"),
    UPDATE_STUDENT_BY_DELETE_COURSE(24, "For deleting course from student"),
    DELETE_STUDENT_BY_ID(25, "For deleting student by id"),
    DELETE_STUDENT_BY_NAME(26, "For deleting student by first and last names"),
    EXIT(0, "For quit the application");

    private final int number;
    private final String description;

    MenuItemEnum(int number, String description) {
        this.number = number;
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("%d - %s", getNumber(), getDescription());
    }
}
