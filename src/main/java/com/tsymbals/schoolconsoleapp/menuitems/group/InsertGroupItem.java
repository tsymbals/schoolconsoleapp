package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class InsertGroupItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a new group name for saving in format [LL-DD] (L - letter, D - digit):";
    private static final String INPUT_ERROR_MESSAGE = "New group name can't be empty, and match the patter [LL-DD] (L - letter, D - digit)";

    private final GroupService groupService;

    @Setter
    private InputValidationService inputValidationService;

    private String groupName;
    private GroupDto groupDto;

    public InsertGroupItem(GroupService groupService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.SAVE_NEW_GROUP.getNumber());
        super.setDescription(MenuItemEnum.SAVE_NEW_GROUP.getDescription());

        this.groupService = groupService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            groupDto = getGroupDto();
            groupService.saveGroup(groupDto);
            log.info("Saving new group with name='{}' successful complete", groupName);
        } catch (IllegalArgumentException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private GroupDto getGroupDto() {
        return new GroupDto(getGroupName());
    }

    private String getGroupName() {
        do {
            groupName = inputValidationService.getValidString(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (!isNameMatchPattern(groupName) || groupName.isBlank());
        return groupName;
    }

    private boolean isNameMatchPattern(String name) {
        return inputValidationService.isGroupNameMatchPattern(name, INPUT_ERROR_MESSAGE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        InsertGroupItem that = (InsertGroupItem) obj;
        return Objects.equals(groupService, that.groupService) && Objects.equals(groupName, that.groupName) && Objects.equals(groupDto, that.groupDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupService, groupName, groupDto);
    }
}
