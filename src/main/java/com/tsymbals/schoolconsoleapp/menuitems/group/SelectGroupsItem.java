package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class SelectGroupsItem extends MenuItem {

    private static final String DELIMITER = "\n";
    private static final String LEFT_BRACKET = "[";
    private static final String RIGHT_BRACKET = "]";
    private static final String EMPTY = "";

    private final GroupService groupService;

    @Getter
    private List<GroupDto> groupsDto;

    public SelectGroupsItem(GroupService groupService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_GROUPS.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_GROUPS.getDescription());

        this.groupService = groupService;
        this.groupsDto = new ArrayList<>();
    }

    @Override
    public void doAction() {
        try {
            groupsDto = groupService.getGroups();
            if (groupsDto.isEmpty()) {
                log.info("Any group doesn't exists");
            } else {
                log.info(getResult(groupsDto));
            }
        } catch (IllegalArgumentException | GroupNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getResult(List<GroupDto> groupsDto) {
        return String.format("%s%s", DELIMITER, List.of(groupsDto
                        .stream()
                        .map(GroupDto::toString)
                        .collect(Collectors.joining(DELIMITER)))
                .toString()
                .replace(LEFT_BRACKET, EMPTY)
                .replace(RIGHT_BRACKET, EMPTY));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectGroupsItem that = (SelectGroupsItem) obj;
        return Objects.equals(groupService, that.groupService) && Objects.equals(groupsDto, that.groupsDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupService, groupsDto);
    }
}
