package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DeleteGroupByNameItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a group name for deleting in format [LL-DD] (L - letter, D - digit):";
    private static final String INPUT_ERROR_MESSAGE = "Group name can't be empty, and match the patter [LL-DD] (L - letter, D - digit)";

    private final GroupService groupService;

    @Setter
    private InputValidationService inputValidationService;

    private String groupName;

    public DeleteGroupByNameItem(GroupService groupService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DELETE_GROUP_BY_NAME.getNumber());
        super.setDescription(MenuItemEnum.DELETE_GROUP_BY_NAME.getDescription());

        this.groupService = groupService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            groupService.deleteGroup(getGroupName());
            log.info("Deleting group with name='{}' successful complete", groupName);
        } catch (IllegalArgumentException | GroupNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getGroupName() {
        do {
            groupName = inputValidationService.getValidString(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (!isNameMatchPattern(groupName) || groupName.isBlank());
        return groupName;
    }

    private boolean isNameMatchPattern(String name) {
        return inputValidationService.isGroupNameMatchPattern(name, INPUT_ERROR_MESSAGE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        DeleteGroupByNameItem that = (DeleteGroupByNameItem) obj;
        return Objects.equals(groupService, that.groupService) && Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupService, groupName);
    }
}
