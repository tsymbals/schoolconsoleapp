package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SelectGroupByIdItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a group id for displaying:";
    private static final String INPUT_ERROR_MESSAGE = "Group id must be positive integer number";
    private static final Long INITIAL_ID = 0L;

    private final GroupService groupService;

    @Setter
    private InputValidationService inputValidationService;

    private Long groupId;

    @Getter
    private GroupDto groupDto;

    public SelectGroupByIdItem(GroupService groupService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_GROUP_BY_ID.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_GROUP_BY_ID.getDescription());

        this.groupService = groupService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            groupDto = groupService.getGroup(getGroupId());
            log.info(getResult(groupDto));
        } catch (IllegalArgumentException | GroupNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getGroupId() {
        do {
            groupId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (groupId <= INITIAL_ID);
        return groupId;
    }

    private String getResult(GroupDto groupDto) {
        return groupDto.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectGroupByIdItem that = (SelectGroupByIdItem) obj;
        return Objects.equals(groupService, that.groupService) && Objects.equals(groupId, that.groupId) && Objects.equals(groupDto, that.groupDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupService, groupId, groupDto);
    }
}
