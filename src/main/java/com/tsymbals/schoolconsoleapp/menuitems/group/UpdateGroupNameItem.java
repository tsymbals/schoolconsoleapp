package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.menuitems.MenuItem;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateGroupNameItem extends MenuItem {

    private static final String ID_INPUT_INFO_MESSAGE = "Enter a group id for updating:";
    private static final String ID_INPUT_ERROR_MESSAGE = "Group id must be positive integer number";
    private static final String NAME_INPUT_INFO_MESSAGE = "Enter a new name for group in format [LL-DD] (L - letter, D - digit):";
    private static final String NAME_INPUT_ERROR_MESSAGE = "New group name can't be empty, and match the patter [LL-DD] (L - letter, D - digit)";
    private static final Long INITIAL_ID = 0L;

    private final GroupService groupService;

    @Setter
    private InputValidationService inputValidationService;

    private Long groupId;
    private String groupName;

    public UpdateGroupNameItem(GroupService groupService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_GROUP_NAME.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_GROUP_NAME.getDescription());

        this.groupService = groupService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            groupService.updateGroupName(getGroupId(), getGroupName());
            log.info("Updating name='{}' at group with id={} successful complete", groupName, groupId);
        } catch (IllegalArgumentException | GroupNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getGroupId() {
        do {
            groupId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), ID_INPUT_INFO_MESSAGE, ID_INPUT_ERROR_MESSAGE);
        } while (groupId <= INITIAL_ID);
        return groupId;
    }

    private String getGroupName() {
        do {
            groupName = inputValidationService.getValidString(new Scanner(System.in), NAME_INPUT_INFO_MESSAGE, NAME_INPUT_ERROR_MESSAGE);
        } while (!isNameMatchPattern(groupName) || groupName.isBlank());
        return groupName;
    }

    private boolean isNameMatchPattern(String name) {
        return inputValidationService.isGroupNameMatchPattern(name, NAME_INPUT_ERROR_MESSAGE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateGroupNameItem that = (UpdateGroupNameItem) obj;
        return Objects.equals(groupService, that.groupService) && Objects.equals(groupId, that.groupId) && Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), groupService, groupId, groupName);
    }
}
