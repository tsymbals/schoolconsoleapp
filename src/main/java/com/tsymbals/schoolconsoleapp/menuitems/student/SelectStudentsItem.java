package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class SelectStudentsItem extends MenuItem {

    private static final String DELIMITER = "\n";
    private static final String LEFT_BRACKET = "[";
    private static final String RIGHT_BRACKET = "]";
    private static final String EMPTY = "";

    private final StudentService studentService;

    @Getter
    private List<StudentDto> studentsDto;

    public SelectStudentsItem(StudentService studentService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_STUDENTS.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_STUDENTS.getDescription());

        this.studentService = studentService;
        this.studentsDto = new ArrayList<>();
    }

    @Override
    public void doAction() {
        try {
            studentsDto = studentService.getStudents();
            if (studentsDto.isEmpty()) {
                log.info("Any student doesn't exists");
            } else {
                log.info(getResult(studentsDto));
            }
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getResult(List<StudentDto> studentsDto) {
        return String.format("%s%s", DELIMITER, List.of(studentsDto
                        .stream()
                        .map(StudentDto::toString)
                        .collect(Collectors.joining(DELIMITER)))
                .toString()
                .replace(LEFT_BRACKET, EMPTY)
                .replace(RIGHT_BRACKET, EMPTY));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectStudentsItem that = (SelectStudentsItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentsDto, that.studentsDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentsDto);
    }
}
