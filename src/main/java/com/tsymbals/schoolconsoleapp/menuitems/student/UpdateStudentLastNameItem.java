package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateStudentLastNameItem extends MenuItem {

    private static final String ID_INPUT_INFO_MESSAGE = "Enter a student id for updating:";
    private static final String ID_INPUT_ERROR_MESSAGE = "Student id must be positive integer number";
    private static final String LAST_NAME_INPUT_INFO_MESSAGE = "Enter a new student last name for updating:";
    private static final String LAST_NAME_INPUT_ERROR_MESSAGE = "New student last name can't be empty";
    private static final Long INITIAL_ID = 0L;

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private Long studentId;
    private String studentLastName;

    public UpdateStudentLastNameItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_STUDENT_LAST_NAME.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_STUDENT_LAST_NAME.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentService.updateStudentLastName(getStudentId(), getStudentLastName());
            log.info("Updating last_name='{}' at student with id={} successful complete", studentLastName, studentId);
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getStudentId() {
        do {
            studentId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), ID_INPUT_INFO_MESSAGE, ID_INPUT_ERROR_MESSAGE);
        } while (studentId <= INITIAL_ID);
        return studentId;
    }

    private String getStudentLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), LAST_NAME_INPUT_INFO_MESSAGE, LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateStudentLastNameItem that = (UpdateStudentLastNameItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentId, that.studentId) && Objects.equals(studentLastName, that.studentLastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentId, studentLastName);
    }
}
