package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.exception.*;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateStudentByAddGroupItem extends MenuItem {

    private static final String STUDENT_FIRST_NAME_INPUT_INFO_MESSAGE = "Enter a student first name for adding group:";
    private static final String STUDENT_FIRST_NAME_INPUT_ERROR_MESSAGE = "Student first name can't be empty";
    private static final String STUDENT_LAST_NAME_INPUT_INFO_MESSAGE = "Enter a student last name for adding group:";
    private static final String STUDENT_LAST_NAME_INPUT_ERROR_MESSAGE = "Student last name can't be empty";
    private static final String GROUP_NAME_INPUT_INFO_MESSAGE = "Enter a group name for adding to student in format [LL-DD] (L - letter, D - digit):";
    private static final String GROUP_NAME_INPUT_ERROR_MESSAGE = "Group name can't be empty, and match the patter [LL-DD] (L - letter, D - digit)";

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private String studentFirstName;
    private String studentLastName;
    private String groupName;

    public UpdateStudentByAddGroupItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_STUDENT_BY_ADD_GROUP.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_STUDENT_BY_ADD_GROUP.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentService.updateStudentByAddingGroup(getStudentFirstName(), getStudentLastName(), getGroupName());
            log.info("Adding group with name='{}' to student with first_name='{}', and last_name='{}' successful complete", groupName, studentFirstName, studentLastName);
        } catch (IllegalArgumentException | GroupNotFoundException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getStudentFirstName() {
        do {
            studentFirstName = inputValidationService.getValidString(new Scanner(System.in), STUDENT_FIRST_NAME_INPUT_INFO_MESSAGE, STUDENT_FIRST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentFirstName.isBlank());
        return studentFirstName;
    }

    private String getStudentLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), STUDENT_LAST_NAME_INPUT_INFO_MESSAGE, STUDENT_LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    private String getGroupName() {
        do {
            groupName = inputValidationService.getValidString(new Scanner(System.in), GROUP_NAME_INPUT_INFO_MESSAGE, GROUP_NAME_INPUT_ERROR_MESSAGE);
        } while (!isNameMatchPattern(groupName) || groupName.isBlank());
        return groupName;
    }

    private boolean isNameMatchPattern(String name) {
        return inputValidationService.isGroupNameMatchPattern(name, GROUP_NAME_INPUT_ERROR_MESSAGE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateStudentByAddGroupItem that = (UpdateStudentByAddGroupItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentFirstName, that.studentFirstName) && Objects.equals(studentLastName, that.studentLastName)
                && Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentFirstName, studentLastName, groupName);
    }
}
