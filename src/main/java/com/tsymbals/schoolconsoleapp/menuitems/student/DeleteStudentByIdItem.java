package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DeleteStudentByIdItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a student id for deleting:";
    private static final String INPUT_ERROR_MESSAGE = "Student id must be positive integer number";
    private static final Long INITIAL_ID = 0L;

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private Long studentId;

    public DeleteStudentByIdItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DELETE_STUDENT_BY_ID.getNumber());
        super.setDescription(MenuItemEnum.DELETE_STUDENT_BY_ID.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentService.deleteStudent(getStudentId());
            log.info("Deleting student with id={} successful complete", studentId);
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getStudentId() {
        do {
            studentId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (studentId <= INITIAL_ID);
        return studentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        DeleteStudentByIdItem that = (DeleteStudentByIdItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentId, that.studentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentId);
    }
}
