package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class InsertStudentItem extends MenuItem {

    private static final String FIRST_NAME_INPUT_INFO_MESSAGE = "Enter a new student first name for saving:";
    private static final String FIRST_NAME_INPUT_ERROR_MESSAGE = "New student first name can't be empty";
    private static final String LAST_NAME_INPUT_INFO_MESSAGE = "Enter a new student last name for saving:";
    private static final String LAST_NAME_INPUT_ERROR_MESSAGE = "New student last name can't be empty";

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private String studentFirstName;
    private String studentLastName;
    private StudentDto studentDto;

    public InsertStudentItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.SAVE_NEW_STUDENT.getNumber());
        super.setDescription(MenuItemEnum.SAVE_NEW_STUDENT.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentDto = getStudentDto();
            studentService.saveStudent(studentDto);
            log.info("Saving new student with first_name='{}', and last_name='{}' successful complete", studentFirstName, studentLastName);
        } catch (IllegalArgumentException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private StudentDto getStudentDto() {
        return new StudentDto(getStudentFirstName(), getStudentLastName());
    }

    private String getStudentFirstName() {
        do {
            studentFirstName = inputValidationService.getValidString(new Scanner(System.in), FIRST_NAME_INPUT_INFO_MESSAGE, FIRST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentFirstName.isBlank());
        return studentFirstName;
    }

    private String getStudentLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), LAST_NAME_INPUT_INFO_MESSAGE, LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        InsertStudentItem that = (InsertStudentItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentFirstName, that.studentFirstName) && Objects.equals(studentLastName, that.studentLastName)
                && Objects.equals(studentDto, that.studentDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentFirstName, studentLastName, studentDto);
    }
}
