package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DeleteStudentByNameItem extends MenuItem {

    private static final String FIRST_NAME_INPUT_INFO_MESSAGE = "Enter a student first name for deleting:";
    private static final String FIRST_NAME_INPUT_ERROR_MESSAGE = "Student first name can't be empty";
    private static final String LAST_NAME_INPUT_INFO_MESSAGE = "Enter a student last name for deleting:";
    private static final String LAST_NAME_INPUT_ERROR_MESSAGE = "Student last name can't be empty";

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private String studentFirstName;
    private String studentLastName;

    public DeleteStudentByNameItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DELETE_STUDENT_BY_NAME.getNumber());
        super.setDescription(MenuItemEnum.DELETE_STUDENT_BY_NAME.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentService.deleteStudent(getStudentFirstName(), getStudentLastName());
            log.info("Deleting student with first_name='{}', and last_name='{}' successful complete", studentFirstName, studentLastName);
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getStudentFirstName() {
        do {
            studentFirstName = inputValidationService.getValidString(new Scanner(System.in), FIRST_NAME_INPUT_INFO_MESSAGE, FIRST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentFirstName.isBlank());
        return studentFirstName;
    }

    private String getStudentLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), LAST_NAME_INPUT_INFO_MESSAGE, LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        DeleteStudentByNameItem that = (DeleteStudentByNameItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentFirstName, that.studentFirstName) && Objects.equals(studentLastName, that.studentLastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentFirstName, studentLastName);
    }
}
