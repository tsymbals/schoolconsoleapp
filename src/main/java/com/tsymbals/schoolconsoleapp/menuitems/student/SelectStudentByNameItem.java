package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SelectStudentByNameItem extends MenuItem {

    private static final String FIRST_NAME_INPUT_INFO_MESSAGE = "Enter a student first name for displaying:";
    private static final String FIRST_NAME_INPUT_ERROR_MESSAGE = "Student first name can't be empty";
    private static final String LAST_NAME_INPUT_INFO_MESSAGE = "Enter a student last name for displaying:";
    private static final String LAST_NAME_INPUT_ERROR_MESSAGE = "Student last name can't be empty";

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private String studentFirstName;
    private String studentLastName;

    @Getter
    private StudentDto studentDto;

    public SelectStudentByNameItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_STUDENT_BY_NAME.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_STUDENT_BY_NAME.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentDto = studentService.getStudent(getStudentFirstName(), getStringLastName());
            log.info(getResult(studentDto));
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getStudentFirstName() {
        do {
            studentFirstName = inputValidationService.getValidString(new Scanner(System.in), FIRST_NAME_INPUT_INFO_MESSAGE, FIRST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentFirstName.isBlank());
        return studentFirstName;
    }

    private String getStringLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), LAST_NAME_INPUT_INFO_MESSAGE, LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    private String getResult(StudentDto studentDto) {
        return studentDto.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectStudentByNameItem that = (SelectStudentByNameItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentFirstName, that.studentFirstName) && Objects.equals(studentLastName, that.studentLastName)
                && Objects.equals(studentDto, that.studentDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentFirstName, studentLastName, studentDto);
    }
}
