package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.exception.*;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateStudentByDeleteCourseItem extends MenuItem {

    private static final String STUDENT_FIRST_NAME_INPUT_INFO_MESSAGE = "Enter a student first name for deleting course:";
    private static final String STUDENT_FIRST_NAME_INPUT_ERROR_MESSAGE = "Student first name can't be empty";
    private static final String STUDENT_LAST_NAME_INPUT_INFO_MESSAGE = "Enter a student last name for deleting course:";
    private static final String STUDENT_LAST_NAME_INPUT_ERROR_MESSAGE = "Student last name can't be empty";
    private static final String COURSE_NAME_INPUT_INFO_MESSAGE = "Enter a course name for deleting from student:";
    private static final String COURSE_NAME_INPUT_ERROR_MESSAGE = "Course name can't be empty";

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private String studentFirstName;
    private String studentLastName;
    private String courseName;

    public UpdateStudentByDeleteCourseItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_STUDENT_BY_DELETE_COURSE.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_STUDENT_BY_DELETE_COURSE.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentService.updateStudentByDeletingCourse(getStudentFirstName(), getStudentLastName(), getCourseName());
            log.info("Deleting course with name='{}' from student with first_name='{}', and last_name='{}' successful complete", courseName, studentFirstName, studentLastName);
        } catch (IllegalArgumentException | CourseNotFoundException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getStudentFirstName() {
        do {
            studentFirstName = inputValidationService.getValidString(new Scanner(System.in), STUDENT_FIRST_NAME_INPUT_INFO_MESSAGE, STUDENT_FIRST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentFirstName.isBlank());
        return studentFirstName;
    }

    private String getStudentLastName() {
        do {
            studentLastName = inputValidationService.getValidString(new Scanner(System.in), STUDENT_LAST_NAME_INPUT_INFO_MESSAGE, STUDENT_LAST_NAME_INPUT_ERROR_MESSAGE);
        } while (studentLastName.isBlank());
        return studentLastName;
    }

    private String getCourseName() {
        do {
            courseName = inputValidationService.getValidString(new Scanner(System.in), COURSE_NAME_INPUT_INFO_MESSAGE, COURSE_NAME_INPUT_ERROR_MESSAGE);
        } while (courseName.isBlank());
        return courseName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateStudentByDeleteCourseItem that = (UpdateStudentByDeleteCourseItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentFirstName, that.studentFirstName) && Objects.equals(studentLastName, that.studentLastName)
                && Objects.equals(courseName, that.courseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentFirstName, studentLastName, courseName);
    }
}
