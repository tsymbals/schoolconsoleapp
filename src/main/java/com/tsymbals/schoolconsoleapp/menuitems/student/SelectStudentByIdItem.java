package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SelectStudentByIdItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a student id for displaying:";
    private static final String INPUT_ERROR_MESSAGE = "Student id must be positive integer number";
    private static final Long INITIAL_ID = 0L;

    private final StudentService studentService;

    @Setter
    private InputValidationService inputValidationService;

    private Long studentId;

    @Getter
    private StudentDto studentDto;

    public SelectStudentByIdItem(StudentService studentService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_STUDENT_BY_ID.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_STUDENT_BY_ID.getDescription());

        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            studentDto = studentService.getStudent(getStudentId());
            log.info(getResult(studentDto));
        } catch (IllegalArgumentException | StudentNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getStudentId() {
        do {
            studentId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (studentId <= INITIAL_ID);
        return studentId;
    }

    private String getResult(StudentDto studentDto) {
        return studentDto.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectStudentByIdItem that = (SelectStudentByIdItem) obj;
        return Objects.equals(studentService, that.studentService) && Objects.equals(studentId, that.studentId) && Objects.equals(studentDto, that.studentDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), studentService, studentId, studentDto);
    }
}
