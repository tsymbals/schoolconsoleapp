package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateCourseNameItem extends MenuItem {

    private static final String ID_INPUT_INFO_MESSAGE = "Enter a course id for updating:";
    private static final String ID_INPUT_ERROR_MESSAGE = "Course id must be positive integer number";
    private static final String NAME_INPUT_INFO_MESSAGE = "Enter a new name for course:";
    private static final String NAME_INPUT_ERROR_MESSAGE = "New name for course can't be empty";
    private static final Long INITIAL_ID = 0L;

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private Long courseId;
    private String courseName;

    public UpdateCourseNameItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_COURSE_NAME.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_COURSE_NAME.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseService.updateCourseName(getCourseId(), getCourseName());
            log.info("Updating name='{}' at course with id={} successful complete", courseName, courseId);
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getCourseId() {
        do {
            courseId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), ID_INPUT_INFO_MESSAGE, ID_INPUT_ERROR_MESSAGE);
        } while (courseId <= INITIAL_ID);
        return courseId;
    }

    private String getCourseName() {
        do {
            courseName = inputValidationService.getValidString(new Scanner(System.in), NAME_INPUT_INFO_MESSAGE, NAME_INPUT_ERROR_MESSAGE);
        } while (courseName.isBlank());
        return courseName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateCourseNameItem that = (UpdateCourseNameItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseId, that.courseId) && Objects.equals(courseName, that.courseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseId, courseName);
    }
}
