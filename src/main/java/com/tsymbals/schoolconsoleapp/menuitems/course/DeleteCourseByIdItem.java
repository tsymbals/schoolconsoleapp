package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DeleteCourseByIdItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a course id for deleting:";
    private static final String INPUT_ERROR_MESSAGE = "Course id must be positive integer number";
    private static final Long INITIAL_ID = 0L;

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private Long courseId;

    public DeleteCourseByIdItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DELETE_COURSE_BY_ID.getNumber());
        super.setDescription(MenuItemEnum.DELETE_COURSE_BY_ID.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseService.deleteCourse(getCourseId());
            log.info("Deleting course with id={} successful complete", courseId);
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getCourseId() {
        do {
            courseId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (courseId <= INITIAL_ID);
        return courseId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        DeleteCourseByIdItem that = (DeleteCourseByIdItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseId);
    }
}
