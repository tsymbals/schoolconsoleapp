package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SelectCourseByIdItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a course id for displaying:";
    private static final String INPUT_ERROR_MESSAGE = "Course id must be positive integer number";
    private static final Long INITIAL_ID = 0L;

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private Long courseId;

    @Getter
    private CourseDto courseDto;

    public SelectCourseByIdItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_COURSE_BY_ID.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_COURSE_BY_ID.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseDto = courseService.getCourse(getCourseId());
            log.info(getResult(courseDto));
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getCourseId() {
        do {
            courseId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (courseId <= INITIAL_ID);
        return courseId;
    }

    private String getResult(CourseDto courseDto) {
        return courseDto.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectCourseByIdItem that = (SelectCourseByIdItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseId, that.courseId) && Objects.equals(courseDto, that.courseDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseId, courseDto);
    }
}
