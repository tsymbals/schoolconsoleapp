package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class UpdateCourseDescriptionItem extends MenuItem {

    private static final String ID_INPUT_INFO_MESSAGE = "Enter a course id for updating:";
    private static final String ID_INPUT_ERROR_MESSAGE = "Course id must be positive integer number";
    private static final String DESCRIPTION_INPUT_INFO_MESSAGE = "Enter a new description for course:";
    private static final String DESCRIPTION_INPUT_ERROR_MESSAGE = "New description for course can't be empty";
    private static final Long INITIAL_ID = 0L;

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private Long courseId;
    private String courseDescription;

    public UpdateCourseDescriptionItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.UPDATE_COURSE_DESCRIPTION.getNumber());
        super.setDescription(MenuItemEnum.UPDATE_COURSE_DESCRIPTION.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseService.updateCourseDescription(getCourseId(), getCourseDescription());
            log.info("Updating description='{}' at course with id={} successful complete", courseDescription, courseId);
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private Long getCourseId() {
        do {
            courseId = inputValidationService.getValidIntegerNumber(new Scanner(System.in), ID_INPUT_INFO_MESSAGE, ID_INPUT_ERROR_MESSAGE);
        } while (courseId <= INITIAL_ID);
        return courseId;
    }

    private String getCourseDescription() {
        do {
            courseDescription = inputValidationService.getValidString(new Scanner(System.in), DESCRIPTION_INPUT_INFO_MESSAGE, DESCRIPTION_INPUT_ERROR_MESSAGE);
        } while (courseDescription.isBlank());
        return courseDescription;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        UpdateCourseDescriptionItem that = (UpdateCourseDescriptionItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseId, that.courseId) && Objects.equals(courseDescription, that.courseDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseId, courseDescription);
    }
}
