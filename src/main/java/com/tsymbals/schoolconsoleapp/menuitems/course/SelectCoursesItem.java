package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class SelectCoursesItem extends MenuItem {

    private static final String DELIMITER = "\n";
    private static final String LEFT_BRACKET = "[";
    private static final String RIGHT_BRACKET = "]";
    private static final String EMPTY = "";

    private final CourseService courseService;

    @Getter
    private List<CourseDto> coursesDto;

    public SelectCoursesItem(CourseService courseService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_COURSES.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_COURSES.getDescription());

        this.courseService = courseService;
        this.coursesDto = new ArrayList<>();
    }

    @Override
    public void doAction() {
        try {
            coursesDto = courseService.getCourses();
            if (coursesDto.isEmpty()) {
                log.info("Any course doesn't exists");
            } else {
                log.info(getResult(coursesDto));
            }
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getResult(List<CourseDto> coursesDto) {
        return String.format("%s%s", DELIMITER, List.of(coursesDto
                        .stream()
                        .map(CourseDto::toString)
                        .collect(Collectors.joining(DELIMITER)))
                .toString()
                .replace(LEFT_BRACKET, EMPTY)
                .replace(RIGHT_BRACKET, EMPTY));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectCoursesItem that = (SelectCoursesItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(coursesDto, that.coursesDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, coursesDto);
    }
}
