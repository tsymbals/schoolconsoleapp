package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class SelectCourseByNameItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a course name for displaying:";
    private static final String INPUT_ERROR_MESSAGE = "Course name can't be empty";

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private String courseName;

    @Getter
    private CourseDto courseDto;

    public SelectCourseByNameItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DISPLAY_COURSE_BY_NAME.getNumber());
        super.setDescription(MenuItemEnum.DISPLAY_COURSE_BY_NAME.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseDto = courseService.getCourse(getCourseName());
            log.info(getResult(courseDto));
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getCourseName() {
        do {
            courseName = inputValidationService.getValidString(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (courseName.isBlank());
        return courseName;
    }

    private String getResult(CourseDto courseDto) {
        return courseDto.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        SelectCourseByNameItem that = (SelectCourseByNameItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseName, that.courseName) && Objects.equals(courseDto, that.courseDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseName, courseDto);
    }
}
