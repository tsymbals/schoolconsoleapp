package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DeleteCourseByNameItem extends MenuItem {

    private static final String INPUT_INFO_MESSAGE = "Enter a course name for deleting:";
    private static final String INPUT_ERROR_MESSAGE = "Course name can't be empty";

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private String courseName;

    public DeleteCourseByNameItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.DELETE_COURSE_BY_NAME.getNumber());
        super.setDescription(MenuItemEnum.DELETE_COURSE_BY_NAME.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseService.deleteCourse(getCourseName());
            log.info("Deleting course with name='{}' successful complete", courseName);
        } catch (IllegalArgumentException | CourseNotFoundException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private String getCourseName() {
        do {
            courseName = inputValidationService.getValidString(new Scanner(System.in), INPUT_INFO_MESSAGE, INPUT_ERROR_MESSAGE);
        } while (courseName.isBlank());
        return courseName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        DeleteCourseByNameItem that = (DeleteCourseByNameItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseName, that.courseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseName);
    }
}
