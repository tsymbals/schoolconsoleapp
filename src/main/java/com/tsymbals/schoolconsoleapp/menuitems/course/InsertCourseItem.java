package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class InsertCourseItem extends MenuItem {

    private static final String NAME_INPUT_INFO_MESSAGE = "Enter a new course name for saving:";
    private static final String NAME_INPUT_ERROR_MESSAGE = "New course name can't be empty";
    private static final String DESCRIPTION_INPUT_INFO_MESSAGE = "Enter a description for new course:";
    private static final String DESCRIPTION_INPUT_ERROR_MESSAGE = "Description for new course can't be empty";

    private final CourseService courseService;

    @Setter
    private InputValidationService inputValidationService;

    private String courseName;
    private String courseDescription;
    private CourseDto courseDto;

    public InsertCourseItem(CourseService courseService, InputValidationService inputValidationService) {
        super();
        super.setNumber(MenuItemEnum.SAVE_NEW_COURSE.getNumber());
        super.setDescription(MenuItemEnum.SAVE_NEW_COURSE.getDescription());

        this.courseService = courseService;
        this.inputValidationService = inputValidationService;
    }

    @Override
    public void doAction() {
        try {
            courseDto = getCourseDto();
            courseService.saveCourse(courseDto);
            log.info("Saving new course with name='{}' and description='{}' successful complete", courseName, courseDescription);
        } catch (IllegalArgumentException e) {
            log.error("Occurred error - {}", e.getMessage());
        }
    }

    private CourseDto getCourseDto() {
        return new CourseDto(getCourseName(), getCourseDescription());
    }

    private String getCourseName() {
        do {
            courseName = inputValidationService.getValidString(new Scanner(System.in), NAME_INPUT_INFO_MESSAGE, NAME_INPUT_ERROR_MESSAGE);
        } while (courseName.isBlank());
        return courseName;
    }

    private String getCourseDescription() {
        do {
            courseDescription = inputValidationService.getValidString(new Scanner(System.in), DESCRIPTION_INPUT_INFO_MESSAGE, DESCRIPTION_INPUT_ERROR_MESSAGE);
        } while (courseDescription.isBlank());
        return courseDescription;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        InsertCourseItem that = (InsertCourseItem) obj;
        return Objects.equals(courseService, that.courseService) && Objects.equals(courseName, that.courseName) && Objects.equals(courseDescription, that.courseDescription)
                && Objects.equals(courseDto, that.courseDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), courseService, courseName, courseDescription, courseDto);
    }
}
