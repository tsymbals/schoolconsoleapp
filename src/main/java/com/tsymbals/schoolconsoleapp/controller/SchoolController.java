package com.tsymbals.schoolconsoleapp.controller;

import com.tsymbals.schoolconsoleapp.menuitems.*;
import com.tsymbals.schoolconsoleapp.menuitems.course.*;
import com.tsymbals.schoolconsoleapp.menuitems.group.*;
import com.tsymbals.schoolconsoleapp.menuitems.student.*;
import com.tsymbals.schoolconsoleapp.service.*;
import com.tsymbals.schoolconsoleapp.service.course.CourseService;
import com.tsymbals.schoolconsoleapp.service.group.GroupService;
import com.tsymbals.schoolconsoleapp.service.student.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;

import java.util.*;
import java.util.stream.*;

@Controller
@Slf4j
public class SchoolController implements ApplicationRunner {

    private static final String INPUT_ERROR_MESSAGE = String.format("Occurred error - Action number must be between %d and %d",
            MenuItemEnum.EXIT.getNumber(), MenuItemEnum.DELETE_STUDENT_BY_NAME.getNumber());
    private static final String DELIMITER = "\n";
    private static final String LEFT_BRACKET = "[";
    private static final String RIGHT_BRACKET = "]";
    private static final String EMPTY = "";

    private final ConfigurableApplicationContext context;
    private final GeneratedDataService generatedDataService;
    private final CourseService courseService;
    private final GroupService groupService;
    private final StudentService studentService;
    private final InputValidationService inputValidationService;
    private final Map<Integer, MenuItem> menuItems;

    public SchoolController(ConfigurableApplicationContext context,
                            GeneratedDataService generatedDataService,
                            CourseService courseService,
                            GroupService groupService,
                            StudentService studentService,
                            InputValidationService inputValidationService) {
        this.context = context;
        this.generatedDataService = generatedDataService;
        this.courseService = courseService;
        this.groupService = groupService;
        this.studentService = studentService;
        this.inputValidationService = inputValidationService;
        this.menuItems = getMenuItems();
    }

    @Override
    public void run(ApplicationArguments args) {
        log.info("School console application is starting...");
        generatedDataService.saveCourses(generatedDataService.getCourses());
        generatedDataService.saveGroups(generatedDataService.getGroups());
        generatedDataService.saveStudents(generatedDataService.getStudents());
        generatedDataService.updateStudentGroups();
        generatedDataService.updateStudentCourses();

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                log.info("{}Enter a number of actions:{}", DELIMITER, getMenuItemActions());
                try {
                    int actionNumber = scanner.nextInt();
                    if (actionNumber == MenuItemEnum.EXIT.getNumber()) {
                        log.info("School console application is closing...");
                        context.close();
                        break;
                    } else if (menuItems.containsKey(actionNumber)) {
                        menuItems.get(actionNumber).doAction();
                    } else {
                        log.error(INPUT_ERROR_MESSAGE);
                    }
                } catch (InputMismatchException e) {
                    log.error(INPUT_ERROR_MESSAGE);
                    scanner.next();
                }
            }
        }
    }

    private String getMenuItemActions() {
        return String.format("%s%s", DELIMITER, List.of(String.join(DELIMITER,
                        Arrays.stream(MenuItemEnum.values())
                        .map(MenuItemEnum::toString)
                        .toList()))
                .toString()
                .replace(LEFT_BRACKET, EMPTY)
                .replace(RIGHT_BRACKET, EMPTY));
    }

    private Map<Integer, MenuItem> getMenuItems() {
        return Stream.of(
                Map.entry(MenuItemEnum.SAVE_NEW_COURSE.getNumber(), new InsertCourseItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_COURSES.getNumber(), new SelectCoursesItem(courseService)),
                Map.entry(MenuItemEnum.DISPLAY_COURSE_BY_ID.getNumber(), new SelectCourseByIdItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_COURSE_BY_NAME.getNumber(), new SelectCourseByNameItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_COURSE_NAME.getNumber(), new UpdateCourseNameItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_COURSE_DESCRIPTION.getNumber(), new UpdateCourseDescriptionItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_COURSE_BY_ID.getNumber(), new DeleteCourseByIdItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_COURSE_BY_NAME.getNumber(), new DeleteCourseByNameItem(courseService, inputValidationService)),
                Map.entry(MenuItemEnum.SAVE_NEW_GROUP.getNumber(), new InsertGroupItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_GROUPS.getNumber(), new SelectGroupsItem(groupService)),
                Map.entry(MenuItemEnum.DISPLAY_GROUP_BY_ID.getNumber(), new SelectGroupByIdItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_GROUP_BY_NAME.getNumber(), new SelectGroupByNameItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_GROUP_NAME.getNumber(), new UpdateGroupNameItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_GROUP_BY_ID.getNumber(), new DeleteGroupByIdItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_GROUP_BY_NAME.getNumber(), new DeleteGroupByNameItem(groupService, inputValidationService)),
                Map.entry(MenuItemEnum.SAVE_NEW_STUDENT.getNumber(), new InsertStudentItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_STUDENTS.getNumber(), new SelectStudentsItem(studentService)),
                Map.entry(MenuItemEnum.DISPLAY_STUDENT_BY_ID.getNumber(), new SelectStudentByIdItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.DISPLAY_STUDENT_BY_NAME.getNumber(), new SelectStudentByNameItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_STUDENT_LAST_NAME.getNumber(), new UpdateStudentLastNameItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_STUDENT_BY_ADD_GROUP.getNumber(), new UpdateStudentByAddGroupItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_STUDENT_BY_DELETE_GROUP.getNumber(), new UpdateStudentByDeleteGroupItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_STUDENT_BY_ADD_COURSE.getNumber(), new UpdateStudentByAddCourseItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.UPDATE_STUDENT_BY_DELETE_COURSE.getNumber(), new UpdateStudentByDeleteCourseItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_STUDENT_BY_ID.getNumber(), new DeleteStudentByIdItem(studentService, inputValidationService)),
                Map.entry(MenuItemEnum.DELETE_STUDENT_BY_NAME.getNumber(), new DeleteStudentByNameItem(studentService, inputValidationService))
                )
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
