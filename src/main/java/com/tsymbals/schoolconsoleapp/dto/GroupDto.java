package com.tsymbals.schoolconsoleapp.dto;

import com.tsymbals.schoolconsoleapp.entity.Student;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class GroupDto {

    private static final String GROUP_TEMPLATE = "Group{id=%d, name='%s', students=%s}";
    private static final String STUDENT_TEMPLATE = "Student{id=%d, firstName='%s', lastName='%s'}";
    private static final String DELIMITER = ", ";

    private Long id;
    private String name;
    private Set<Student> students;

    public GroupDto(String name) {
        this.name = name;
        this.students = new HashSet<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        GroupDto groupDto = (GroupDto) obj;
        return Objects.equals(id, groupDto.id) && Objects.equals(name, groupDto.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        if (students.isEmpty()) {
            return String.format(GROUP_TEMPLATE, id, name, students);
        }
        return String.format(GROUP_TEMPLATE, id, name, toString(students));
    }

    private String toString(Set<Student> students) {
        return students.stream()
                .map(this::toString)
                .collect(Collectors.joining(DELIMITER));
    }

    private String toString(Student student) {
        return String.format(STUDENT_TEMPLATE, student.getId(), student.getFirstName(), student.getLastName());
    }
}
