package com.tsymbals.schoolconsoleapp.dto;

import com.tsymbals.schoolconsoleapp.entity.*;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class StudentDto {

    private static final String STUDENT_TEMPLATE = "Student{id=%d, group=%s, firstName='%s', lastName='%s', courses=%s}";
    private static final String GROUP_TEMPLATE = "Group{id=%d, name='%s'}";
    private static final String COURSE_TEMPLATE = "Course{id=%d, name='%s'}";
    private static final String NULL = "null";
    private static final String DELIMITER = ", ";

    private Long id;
    private Group group;
    private String firstName;
    private String lastName;
    private Set<Course> courses;

    public StudentDto(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.courses = new HashSet<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        StudentDto that = (StudentDto) obj;
        return Objects.equals(id, that.id) && Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        if (Objects.isNull(group) && courses.isEmpty()) {
            return String.format(STUDENT_TEMPLATE, id, NULL, firstName, lastName, courses);
        } else if (Objects.isNull(group)) {
            return String.format(STUDENT_TEMPLATE, id, NULL, firstName, lastName, toString(courses));
        } else if (courses.isEmpty()) {
            return String.format(STUDENT_TEMPLATE, id, toString(group), firstName, lastName, courses);
        }
        return String.format(STUDENT_TEMPLATE, id, toString(group), firstName, lastName, toString(courses));
    }

    private String toString(Group group) {
        return String.format(GROUP_TEMPLATE, group.getId(), group.getName());
    }

    private String toString(Set<Course> courses) {
        return courses.stream()
                .map(this::toString)
                .collect(Collectors.joining(DELIMITER));
    }

    private String toString(Course course) {
        return String.format(COURSE_TEMPLATE, course.getId(), course.getName());
    }
}
