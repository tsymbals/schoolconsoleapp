package com.tsymbals.schoolconsoleapp.dto;

import com.tsymbals.schoolconsoleapp.entity.Student;
import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class CourseDto {

    private static final String COURSE_TEMPLATE = "Course{id=%d, name='%s', description='%s', students=%s}";
    private static final String STUDENT_TEMPLATE = "Student{id=%d, firstName='%s', lastName='%s'}";
    private static final String DELIMITER = ", ";

    private Long id;
    private String name;
    private String description;
    private Set<Student> students;

    public CourseDto(String name, String description) {
        this.name = name;
        this.description = description;
        this.students = new HashSet<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        CourseDto courseDto = (CourseDto) obj;
        return Objects.equals(id, courseDto.id) && Objects.equals(name, courseDto.name) && Objects.equals(description, courseDto.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    @Override
    public String toString() {
        if (students.isEmpty()) {
            return String.format(COURSE_TEMPLATE, id, name, description, students);
        }
        return String.format(COURSE_TEMPLATE, id, name, description, toString(students));
    }

    private String toString(Set<Student> students) {
        return students.stream()
                .map(this::toString)
                .collect(Collectors.joining(DELIMITER));
    }

    private String toString(Student student) {
        return String.format(STUDENT_TEMPLATE, student.getId(), student.getFirstName(), student.getLastName());
    }
}
