package com.tsymbals.schoolconsoleapp.service.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.exception.*;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

    private static final String STUDENT_WITH_ID_DOES_NOT_EXISTS = "Student with id=%d doesn't exists";
    private static final String STUDENT_WITH_NAME_DOES_NOT_EXISTS = "Student with first_name='%s', and last_name='%s' doesn't exists";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXISTS = "Group with name='%s' doesn't exists";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXISTS = "Course with name='%s' doesn't exists";

    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final CourseRepository courseRepository;
    private final StudentDtoMapper studentDtoMapper;

    public StudentServiceImpl(StudentRepository studentRepository, GroupRepository groupRepository, CourseRepository courseRepository, StudentDtoMapper studentDtoMapper) {
        this.studentRepository = studentRepository;
        this.groupRepository = groupRepository;
        this.courseRepository = courseRepository;
        this.studentDtoMapper = studentDtoMapper;
    }

    @Override
    @Transactional
    public void saveStudent(StudentDto studentDto) {
        studentRepository.save(studentDtoMapper.toEntity(studentDto));
    }

    @Override
    @Transactional
    public List<StudentDto> getStudents() {
        return Optional.of(studentRepository.findAll(Sort.by("id"))
                        .stream()
                        .map(studentDtoMapper)
                        .toList())
                .filter(students -> !students.isEmpty())
                .orElse(List.of());
    }

    @Override
    @Transactional
    public StudentDto getStudent(Long id) {
        return studentRepository.findById(id)
                .map(studentDtoMapper)
                .orElseThrow(() -> new StudentNotFoundException(String.format(STUDENT_WITH_ID_DOES_NOT_EXISTS, id)));
    }

    @Override
    @Transactional
    public StudentDto getStudent(String firstName, String lastName) {
        return studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .map(studentDtoMapper)
                .orElseThrow(() -> new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName)));
    }

    @Override
    @Transactional
    public void updateStudentLastName(Long id, String lastName) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> {
                    student.setLastName(lastName);
                    studentRepository.save(student);
                }, () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void updateStudentByAddingGroup(String firstName, String lastName, String groupName) {
        studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .ifPresentOrElse(student -> {
                    if (Objects.nonNull(student.getGroup())) {
                        throw new IllegalArgumentException(String.format("Student with first_name='%s', and last_name='%s' already have group", firstName, lastName));
                    } else {
                        groupRepository.findByName(groupName)
                                .ifPresentOrElse(group -> {
                                    student.setGroup(group);
                                    studentRepository.save(student);
                                }, () -> {
                                    throw new GroupNotFoundException(String.format(GROUP_WITH_NAME_DOES_NOT_EXISTS, groupName));
                                });
                    }
                }, () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName));
                });
    }

    @Override
    @Transactional
    public void updateStudentByDeletingGroup(String firstName, String lastName, String groupName) {
        studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .ifPresentOrElse(student -> {
                    if (Objects.isNull(student.getGroup())) {
                        throw new IllegalArgumentException(String.format("Student with first_name='%s', and last_name='%s' doesn't have any group", firstName, lastName));
                    } else {
                        groupRepository.findByName(groupName)
                                .ifPresentOrElse(group -> {
                                    if (student.getGroup().getName().equals(groupName)) {
                                        student.setGroup(null);
                                        studentRepository.save(student);
                                    } else {
                                        throw new IllegalArgumentException(String.format("Student with first_name='%s', and last_name='%s' doesn't have group with name='%s'", firstName, lastName, groupName));
                                    }
                                }, () -> {
                                    throw new GroupNotFoundException(String.format(GROUP_WITH_NAME_DOES_NOT_EXISTS, groupName));
                                });
                    }
                }, () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName));
                });
    }

    @Override
    @Transactional
    public void updateStudentByAddingCourse(String firstName, String lastName, String courseName) {
        studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .ifPresentOrElse(student -> courseRepository.findByName(courseName)
                        .ifPresentOrElse(course -> {
                            if (student.getCourses().contains(course)) {
                                throw new IllegalArgumentException(String.format("Student with first_name='%s', and last_name='%s' already have course with name='%s'", firstName, lastName, courseName));
                            } else {
                                student.getCourses().add(course);
                                studentRepository.save(student);
                            }
                        }, () -> {
                            throw new CourseNotFoundException(String.format(COURSE_WITH_NAME_DOES_NOT_EXISTS, courseName));
                        }), () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName));
                });
    }

    @Override
    @Transactional
    public void updateStudentByDeletingCourse(String firstName, String lastName, String courseName) {
        studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .ifPresentOrElse(student -> courseRepository.findByName(courseName)
                        .ifPresentOrElse(course -> {
                            if (!student.getCourses().contains(course)) {
                                throw new IllegalArgumentException(String.format("Student with first_name='%s', and last_name='%s' doesn't have course with name='%s'", firstName, lastName, courseName));
                            } else {
                                student.getCourses().remove(course);
                                studentRepository.save(student);
                            }
                        }, () -> {
                            throw new CourseNotFoundException(String.format(COURSE_WITH_NAME_DOES_NOT_EXISTS, courseName));
                        }), () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName));
                });
    }

    @Override
    @Transactional
    public void deleteStudent(Long id) {
        studentRepository.findById(id)
                .ifPresentOrElse(studentRepository::delete, () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void deleteStudent(String firstName, String lastName) {
        studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .ifPresentOrElse(studentRepository::delete, () -> {
                    throw new StudentNotFoundException(String.format(STUDENT_WITH_NAME_DOES_NOT_EXISTS, firstName, lastName));
                });
    }
}
