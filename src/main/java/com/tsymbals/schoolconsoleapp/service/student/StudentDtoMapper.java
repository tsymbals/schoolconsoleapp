package com.tsymbals.schoolconsoleapp.service.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.entity.Student;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class StudentDtoMapper implements Function<Student, StudentDto> {

    @Override
    public StudentDto apply(Student student) {
        return new StudentDto(
                student.getId(),
                student.getGroup(),
                student.getFirstName(),
                student.getLastName(),
                student.getCourses()
        );
    }

    public Student toEntity(StudentDto studentDto) {
        return new Student(
                studentDto.getFirstName(),
                studentDto.getLastName()
        );
    }
}
