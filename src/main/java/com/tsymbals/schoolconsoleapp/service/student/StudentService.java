package com.tsymbals.schoolconsoleapp.service.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;

import java.util.List;

public interface StudentService {
    void saveStudent(StudentDto studentDto);
    List<StudentDto> getStudents();
    StudentDto getStudent(Long id);
    StudentDto getStudent(String firstName, String lastName);
    void updateStudentLastName(Long id, String lastName);
    void updateStudentByAddingGroup(String firstName, String lastName, String groupName);
    void updateStudentByDeletingGroup(String firstName, String lastName, String groupName);
    void updateStudentByAddingCourse(String firstName, String lastName, String courseName);
    void updateStudentByDeletingCourse(String firstName, String lastName, String courseName);
    void deleteStudent(Long id);
    void deleteStudent(String firstName, String lastName);
}
