package com.tsymbals.schoolconsoleapp.service.student;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.entity.*;
import com.tsymbals.schoolconsoleapp.service.ReadFromFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.*;

@Service
@Slf4j
public class GeneratedStudentServiceImpl implements GeneratedStudentService {

    private static final int INITIAL_INDEX = 0;
    private static final long INITIAL_ID = 1L;
    private static final long INCREMENTED_MAX_NUMBER_OF_STUDENTS = 201L;
    private static final long INCREMENTED_MAX_NUMBER_OF_COURSES = 11L;
    private static final long MIN_NUMBER_OF_COURSES = 1L;
    private static final long MAX_NUMBER_OF_COURSES = 3L;
    private static final long INCREMENTED_MAX_NUMBER_OF_GROUPS = 11L;
    private static final int MIN_NUMBER_OF_STUDENTS = 10;
    private static final int MAX_NUMBER_OF_STUDENTS = 30;

    private final ReadFromFileService readFromFileService;
    private final Random random;
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final CourseRepository courseRepository;

    public GeneratedStudentServiceImpl(ReadFromFileService readFromFileService, StudentRepository studentRepository, GroupRepository groupRepository, CourseRepository courseRepository) {
        this.readFromFileService = readFromFileService;
        this.random = new Random();
        this.studentRepository = studentRepository;
        this.groupRepository = groupRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Student> getGeneratedStudents() {
        List<String> firstNames = getNames("first");
        List<String> lastNames = getNames("last");

        return LongStream.range(INITIAL_ID, INCREMENTED_MAX_NUMBER_OF_STUDENTS)
                .mapToObj(i -> {
                    Student student = new Student();
                    student.setFirstName(firstNames.get(random.nextInt(firstNames.size())));
                    student.setLastName(lastNames.get(random.nextInt(lastNames.size())));
                    return student;
                })
                .toList();
    }

    @Override
    public void saveGeneratedStudents(List<Student> students) {
        AtomicLong id = new AtomicLong(1);
        students.forEach(student -> {
            student.setId(id.getAndIncrement());
            if (studentRepository.findById(student.getId()).isPresent()) {
                log.info("Student with id={} already exists", student.getId());
            } else {
                student.setId(null);
                studentRepository.save(student);
                log.info("Saving generated student with first_name='{}', and last_name='{}' successful complete", student.getFirstName(), student.getLastName());
            }
        });
    }

    @Override
    public void addGroupsToStudents() {
        List<Student> students = studentRepository.findAll();
        groupRepository.findAll().stream()
                .filter(group -> group.getStudents().isEmpty() && group.getId() < INCREMENTED_MAX_NUMBER_OF_GROUPS)
                .forEach(group -> {
                    IntStream.range(INITIAL_INDEX, random.nextInt((MAX_NUMBER_OF_STUDENTS - MIN_NUMBER_OF_STUDENTS) + 1) + MIN_NUMBER_OF_STUDENTS)
                            .forEach(i -> {
                                Student student = students.get(random.nextInt(students.size()));
                                group.addStudent(student);
                                log.info("Student with id={} was added to group with id={}", student.getId(), group.getId());
                            });
                    groupRepository.save(group);
                });
    }

    @Override
    public void addCoursesToStudents() {
        studentRepository.saveAll(getUpdatedStudents());
    }

    private List<String> getNames(String namesType) {
        List<String> names = readFromFileService.getLines("SchoolStudents.txt");
        return IntStream.range(INITIAL_INDEX, names.size())
                .boxed()
                .collect(Collectors.toMap(
                        index -> (index == INITIAL_INDEX) ? "first" : "last",
                        index -> Arrays.stream(names.get(index).split("\\s+")).toList()))
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().equalsIgnoreCase(namesType.trim()))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(List.of());
    }

    private List<Student> getUpdatedStudents() {
        return studentRepository.findAll().stream()
                .filter(student -> student.getCourses().isEmpty() && student.getId() < INCREMENTED_MAX_NUMBER_OF_STUDENTS)
                .map(student -> {
                    getRandomCourses().forEach(course -> {
                        student.getCourses().add(course);
                        log.info("Course with id={} was added to student with id={}", course.getId(), student.getId());
                    });
                    return student;
                })
                .toList();
    }

    private Set<Course> getRandomCourses() {
        return random.longs(INITIAL_ID, INCREMENTED_MAX_NUMBER_OF_COURSES)
                .distinct()
                .limit(random.nextLong(MAX_NUMBER_OF_COURSES) + MIN_NUMBER_OF_COURSES)
                .boxed()
                .toList()
                .stream()
                .map(courseRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }
}
