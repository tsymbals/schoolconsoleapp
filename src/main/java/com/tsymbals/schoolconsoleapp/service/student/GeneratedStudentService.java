package com.tsymbals.schoolconsoleapp.service.student;

import com.tsymbals.schoolconsoleapp.entity.Student;

import java.util.List;

public interface GeneratedStudentService {
    List<Student> getGeneratedStudents();
    void saveGeneratedStudents(List<Student> students);
    void addGroupsToStudents();
    void addCoursesToStudents();
}
