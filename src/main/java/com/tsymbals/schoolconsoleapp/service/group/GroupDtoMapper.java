package com.tsymbals.schoolconsoleapp.service.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.entity.Group;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class GroupDtoMapper implements Function<Group, GroupDto> {

    @Override
    public GroupDto apply(Group group) {
        return new GroupDto(
                group.getId(),
                group.getName(),
                group.getStudents()
        );
    }

    public Group toEntity(GroupDto groupDto) {
        return new Group(
                groupDto.getName()
        );
    }
}
