package com.tsymbals.schoolconsoleapp.service.group;

import com.tsymbals.schoolconsoleapp.entity.Group;

import java.util.List;

public interface GeneratedGroupService {
    List<Group> getGeneratedGroups();
    void saveGeneratedGroups(List<Group> groups);
}
