package com.tsymbals.schoolconsoleapp.service.group;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.*;

@Service
@Slf4j
public class GeneratedGroupServiceImpl implements GeneratedGroupService {

    private static final long INITIAL_ID = 1L;
    private static final long INCREMENTED_MAX_NUMBER_OF_GROUPS = 11L;
    private static final int LEFT_ALPHABETIC_INCLUSIVE_LIMIT = 65;
    private static final int RIGHT_ALPHABETIC_EXCLUSIVE_LIMIT = 91;
    private static final int TARGET_STRING_LENGTH = 2;
    private static final String DASH = "-";
    private static final int LEFT_NUMERIC_INCLUSIVE_LIMIT = 48;
    private static final int RIGHT_NUMERIC_EXCLUSIVE_LIMIT = 58;

    private final Random random;
    private final GroupRepository groupRepository;

    public GeneratedGroupServiceImpl(GroupRepository groupRepository) {
        this.random = new Random();
        this.groupRepository = groupRepository;
    }

    @Override
    public List<Group> getGeneratedGroups() {
        return LongStream.range(INITIAL_ID, INCREMENTED_MAX_NUMBER_OF_GROUPS)
                .mapToObj(i -> {
                    Group group = new Group();
                    group.setName(generateRandomName());
                    return group;
                })
                .toList();
    }

    @Override
    public void saveGeneratedGroups(List<Group> groups) {
        AtomicLong id = new AtomicLong(1);
        groups.forEach(group -> {
            group.setId(id.getAndIncrement());
            if (groupRepository.findById(group.getId()).isPresent()) {
                log.info("Group with id={} already exists", group.getId());
            } else {
                group.setId(null);
                groupRepository.save(group);
                log.info("Saving generated group with name='{}' successful complete", group.getName());
            }
        });
    }

    private String generateRandomName() {
        return random.ints(LEFT_ALPHABETIC_INCLUSIVE_LIMIT, RIGHT_ALPHABETIC_EXCLUSIVE_LIMIT)
                .limit(TARGET_STRING_LENGTH)
                .mapToObj(c -> String.valueOf((char) c))
                .collect(Collectors.joining())
                .concat(DASH)
                .concat(random.ints(LEFT_NUMERIC_INCLUSIVE_LIMIT, RIGHT_NUMERIC_EXCLUSIVE_LIMIT)
                        .limit(TARGET_STRING_LENGTH)
                        .mapToObj(c -> String.valueOf((char) c))
                        .collect(Collectors.joining()));
    }
}
