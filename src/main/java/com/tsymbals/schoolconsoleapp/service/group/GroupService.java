package com.tsymbals.schoolconsoleapp.service.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;

import java.util.List;

public interface GroupService {
    void saveGroup(GroupDto groupDto);
    List<GroupDto> getGroups();
    GroupDto getGroup(Long id);
    GroupDto getGroup(String name);
    void updateGroupName(Long id, String name);
    void deleteGroup(Long id);
    void deleteGroup(String name);
}
