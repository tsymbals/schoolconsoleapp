package com.tsymbals.schoolconsoleapp.service.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GroupServiceImpl implements GroupService {

    private static final String GROUP_WITH_ID_DOES_NOT_EXISTS = "Group with id=%d doesn't exists";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXISTS = "Group with name='%s' doesn't exists";

    private final GroupRepository groupRepository;
    private final GroupDtoMapper groupDtoMapper;

    public GroupServiceImpl(GroupRepository groupRepository, GroupDtoMapper groupDtoMapper) {
        this.groupRepository = groupRepository;
        this.groupDtoMapper = groupDtoMapper;
    }

    @Override
    @Transactional
    public void saveGroup(GroupDto groupDto) {
        groupRepository.save(groupDtoMapper.toEntity(groupDto));
    }

    @Override
    @Transactional
    public List<GroupDto> getGroups() {
        return Optional.of(groupRepository.findAll(Sort.by("id"))
                        .stream()
                        .map(groupDtoMapper)
                        .toList())
                .filter(groups -> !groups.isEmpty())
                .orElse(List.of());
    }

    @Override
    @Transactional
    public GroupDto getGroup(Long id) {
        return groupRepository.findById(id)
                .map(groupDtoMapper)
                .orElseThrow(() -> new GroupNotFoundException(String.format(GROUP_WITH_ID_DOES_NOT_EXISTS, id)));
    }

    @Override
    @Transactional
    public GroupDto getGroup(String name) {
        return groupRepository.findByName(name)
                .map(groupDtoMapper)
                .orElseThrow(() -> new GroupNotFoundException(String.format(GROUP_WITH_NAME_DOES_NOT_EXISTS, name)));
    }

    @Override
    @Transactional
    public void updateGroupName(Long id, String name) {
        groupRepository.findById(id)
                .ifPresentOrElse(group -> {
                    group.setName(name);
                    groupRepository.save(group);
                }, () -> {
                    throw new GroupNotFoundException(String.format(GROUP_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void deleteGroup(Long id) {
        groupRepository.findById(id)
                .ifPresentOrElse(groupRepository::delete, () -> {
                    throw new GroupNotFoundException(String.format(GROUP_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void deleteGroup(String name) {
        groupRepository.findByName(name)
                .ifPresentOrElse(groupRepository::delete, () -> {
                    throw new GroupNotFoundException(String.format(GROUP_WITH_NAME_DOES_NOT_EXISTS, name));
                });
    }
}
