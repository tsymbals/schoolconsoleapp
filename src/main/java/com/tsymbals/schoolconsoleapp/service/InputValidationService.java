package com.tsymbals.schoolconsoleapp.service;

import java.util.Scanner;

public interface InputValidationService {
    long getValidIntegerNumber(Scanner scanner, String infoMessage, String errorMessage);
    String getValidString(Scanner scanner, String infoMessage, String errorMessage);
    boolean isGroupNameMatchPattern(String input, String errorMessage);
}
