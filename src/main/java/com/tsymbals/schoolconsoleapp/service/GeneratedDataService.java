package com.tsymbals.schoolconsoleapp.service;

import com.tsymbals.schoolconsoleapp.entity.*;

import java.util.List;

public interface GeneratedDataService {
    List<Course> getCourses();
    List<Group> getGroups();
    List<Student> getStudents();
    void saveCourses(List<Course> courses);
    void saveGroups(List<Group> groups);
    void saveStudents(List<Student> students);
    void updateStudentGroups();
    void updateStudentCourses();
}
