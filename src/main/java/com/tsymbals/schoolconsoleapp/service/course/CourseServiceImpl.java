package com.tsymbals.schoolconsoleapp.service.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CourseServiceImpl implements CourseService {

    private static final String COURSE_WITH_ID_DOES_NOT_EXISTS = "Course with id=%d doesn't exists";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXISTS = "Course with name='%s' doesn't exists";

    private final CourseRepository courseRepository;
    private final CourseDtoMapper courseDtoMapper;

    public CourseServiceImpl(CourseRepository courseRepository, CourseDtoMapper courseDtoMapper) {
        this.courseRepository = courseRepository;
        this.courseDtoMapper = courseDtoMapper;
    }

    @Override
    @Transactional
    public void saveCourse(CourseDto courseDto) {
        courseRepository.save(courseDtoMapper.toEntity(courseDto));
    }

    @Override
    @Transactional
    public List<CourseDto> getCourses() {
        return Optional.of(courseRepository.findAll(Sort.by("id"))
                        .stream()
                        .map(courseDtoMapper)
                        .toList())
                .filter(courses -> !courses.isEmpty())
                .orElse(List.of());
    }

    @Override
    @Transactional
    public CourseDto getCourse(Long id) {
        return courseRepository.findById(id)
                .map(courseDtoMapper)
                .orElseThrow(() -> new CourseNotFoundException(String.format(COURSE_WITH_ID_DOES_NOT_EXISTS, id)));
    }

    @Override
    @Transactional
    public CourseDto getCourse(String name) {
        return courseRepository.findByName(name)
                .map(courseDtoMapper)
                .orElseThrow(() -> new CourseNotFoundException(String.format(COURSE_WITH_NAME_DOES_NOT_EXISTS, name)));
    }

    @Override
    @Transactional
    public void updateCourseName(Long id, String name) {
        courseRepository.findById(id)
                .ifPresentOrElse(course -> {
                    course.setName(name);
                    courseRepository.save(course);
                }, () -> {
                    throw new CourseNotFoundException(String.format(COURSE_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void updateCourseDescription(Long id, String description) {
        courseRepository.findById(id)
                .ifPresentOrElse(course -> {
                    course.setDescription(description);
                    courseRepository.save(course);
                }, () -> {
                    throw new CourseNotFoundException(String.format(COURSE_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void deleteCourse(Long id) {
        courseRepository.findById(id)
                .ifPresentOrElse(courseRepository::delete, () -> {
                    throw new CourseNotFoundException(String.format(COURSE_WITH_ID_DOES_NOT_EXISTS, id));
                });
    }

    @Override
    @Transactional
    public void deleteCourse(String name) {
        courseRepository.findByName(name)
                .ifPresentOrElse(courseRepository::delete, () -> {
                    throw new CourseNotFoundException(String.format(COURSE_WITH_NAME_DOES_NOT_EXISTS, name));
                });
    }
}
