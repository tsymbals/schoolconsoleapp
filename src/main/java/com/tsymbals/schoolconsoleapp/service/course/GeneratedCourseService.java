package com.tsymbals.schoolconsoleapp.service.course;

import com.tsymbals.schoolconsoleapp.entity.Course;

import java.util.List;

public interface GeneratedCourseService {
    List<Course> getGeneratedCourses();
    void saveGeneratedCourses(List<Course> courses);
}
