package com.tsymbals.schoolconsoleapp.service.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;

import java.util.List;

public interface CourseService {
    void saveCourse(CourseDto courseDto);
    List<CourseDto> getCourses();
    CourseDto getCourse(Long id);
    CourseDto getCourse(String name);
    void updateCourseName(Long id, String name);
    void updateCourseDescription(Long id, String description);
    void deleteCourse(Long id);
    void deleteCourse(String name);
}
