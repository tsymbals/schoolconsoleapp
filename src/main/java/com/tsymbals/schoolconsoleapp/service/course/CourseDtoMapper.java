package com.tsymbals.schoolconsoleapp.service.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.entity.Course;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CourseDtoMapper implements Function<Course, CourseDto> {

    @Override
    public CourseDto apply(Course course) {
        return new CourseDto(
                course.getId(),
                course.getName(),
                course.getDescription(),
                course.getStudents()
        );
    }

    public Course toEntity(CourseDto courseDto) {
        return new Course(
                courseDto.getName(),
                courseDto.getDescription()
        );
    }
}
