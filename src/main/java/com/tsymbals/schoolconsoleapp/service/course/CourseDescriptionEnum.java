package com.tsymbals.schoolconsoleapp.service.course;

import lombok.Getter;

@Getter
public enum CourseDescriptionEnum {

    ALGEBRA("Algebra is the study of variables and the rules for manipulating these variables in formulas."),
    ANTHROPOLOGY("Anthropology is the scientific study of humanity, concerned with human behavior, human biology etc."),
    BIOLOGY("Biology is the scientific study of life."),
    CHEMISTRY("Chemistry is the scientific study of the properties and behavior of matter."),
    GEOGRAPHY("Geography is a field of science devoted to the study of the lands, features, inhabitants, and phenomena of Earth."),
    GEOMETRY("Geometry is a branch of mathematics concerned with properties of space such as the distance, shape, size etc."),
    HUMANITIES("Humanities are academic discipline that study aspects of human society and culture."),
    PHYSICS("Physics is the natural science of matter, involving the study of matter."),
    PSYCHOLOGY("Psychology is the study of mind and behavior in humans and non-humans."),
    WORLD_HISTORY("World history or global history as a field of historical study examines history from a global perspective.");

    private final String description;

    CourseDescriptionEnum(String description) {
        this.description = description;
    }
}
