package com.tsymbals.schoolconsoleapp.service.course;

import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.service.ReadFromFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class GeneratedCourseServiceImpl implements GeneratedCourseService {

    private final ReadFromFileService readFromFileService;
    private final CourseRepository courseRepository;

    public GeneratedCourseServiceImpl(ReadFromFileService readFromFileService, CourseRepository courseRepository) {
        this.readFromFileService = readFromFileService;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getGeneratedCourses() {
        return readFromFileService.getLines("SchoolCourses.txt")
                .stream()
                .map(name -> {
                    Course course = new Course();
                    course.setName(name);
                    Arrays.stream(CourseDescriptionEnum.values())
                            .filter(descriptionEnum -> descriptionEnum.name().equalsIgnoreCase(name))
                            .findFirst()
                            .ifPresent(descriptionEnum -> course.setDescription(descriptionEnum.getDescription()));
                    return course;
                })
                .toList();
    }

    @Override
    public void saveGeneratedCourses(List<Course> courses) {
        courses.forEach(course -> {
            if (courseRepository.findByName(course.getName()).isPresent()) {
                log.info("Course with name='{}' already exists", course.getName());
            } else {
                courseRepository.save(course);
                log.info("Saving generated course with name='{}' and description='{}' successful complete", course.getName(), course.getDescription());
            }
        });
    }
}
