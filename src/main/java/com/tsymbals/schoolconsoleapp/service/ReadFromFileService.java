package com.tsymbals.schoolconsoleapp.service;

import java.util.List;

public interface ReadFromFileService {
    List<String> getLines(String fileName);
}
