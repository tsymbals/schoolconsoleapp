package com.tsymbals.schoolconsoleapp.service;

import com.tsymbals.schoolconsoleapp.entity.*;
import com.tsymbals.schoolconsoleapp.service.course.GeneratedCourseService;
import com.tsymbals.schoolconsoleapp.service.group.GeneratedGroupService;
import com.tsymbals.schoolconsoleapp.service.student.GeneratedStudentService;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GeneratedDataServiceImpl implements GeneratedDataService {

    private final GeneratedCourseService generatedCourseService;
    private final GeneratedGroupService generatedGroupService;
    private final GeneratedStudentService generatedStudentService;

    public GeneratedDataServiceImpl(GeneratedCourseService generatedCourseService,
                                    GeneratedGroupService generatedGroupService,
                                    GeneratedStudentService generatedStudentService) {
        this.generatedCourseService = generatedCourseService;
        this.generatedGroupService = generatedGroupService;
        this.generatedStudentService = generatedStudentService;
    }

    @Override
    public List<Course> getCourses() {
        return generatedCourseService.getGeneratedCourses();
    }

    @Override
    public List<Group> getGroups() {
        return generatedGroupService.getGeneratedGroups();
    }

    @Override
    public List<Student> getStudents() {
        return generatedStudentService.getGeneratedStudents();
    }

    @Override
    @Transactional
    public void saveCourses(List<Course> courses) {
        generatedCourseService.saveGeneratedCourses(courses);
    }

    @Override
    @Transactional
    public void saveGroups(List<Group> groups) {
        generatedGroupService.saveGeneratedGroups(groups);
    }

    @Override
    @Transactional
    public void saveStudents(List<Student> students) {
        generatedStudentService.saveGeneratedStudents(students);
    }

    @Override
    @Transactional
    public void updateStudentGroups() {
        generatedStudentService.addGroupsToStudents();
    }

    @Override
    @Transactional
    public void updateStudentCourses() {
        generatedStudentService.addCoursesToStudents();
    }
}
