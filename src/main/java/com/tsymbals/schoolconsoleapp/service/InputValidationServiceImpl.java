package com.tsymbals.schoolconsoleapp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.*;

@Service
@Slf4j
public class InputValidationServiceImpl implements InputValidationService {

    private static final String OCCURRED_ERROR = "Occurred error - {}";

    @Override
    public long getValidIntegerNumber(Scanner scanner, String infoMessage, String errorMessage) {
        log.info(infoMessage);
        long id = 0L;
        try {
            id = scanner.nextLong();
            if (id <= 0) {
                log.error(OCCURRED_ERROR, errorMessage);
            } else {
                return id;
            }
        } catch (InputMismatchException e) {
            log.error(OCCURRED_ERROR, errorMessage);
        }
        return id;
    }

    @Override
    public String getValidString(Scanner scanner, String infoMessage, String errorMessage) {
        log.info(infoMessage);
        String str = scanner.nextLine();
        while (str.isBlank()) {
            log.error(OCCURRED_ERROR, errorMessage);
            log.info(infoMessage);
            str = scanner.nextLine();
        }
        return str;
    }

    @Override
    public boolean isGroupNameMatchPattern(String input, String errorMessage) {
        if (Objects.nonNull(input) && !input.isEmpty()) {
            String regularExpression = "[A-Z]{2}-\\d{2}";
            Pattern pattern = Pattern.compile(regularExpression);
            Matcher matcher = pattern.matcher(input);
            if (!matcher.matches()) {
                log.error(OCCURRED_ERROR, errorMessage);
            }
            return matcher.matches();
        }
        return false;
    }
}
