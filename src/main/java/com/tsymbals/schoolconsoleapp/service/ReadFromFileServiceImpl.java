package com.tsymbals.schoolconsoleapp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.*;
import java.nio.file.*;
import java.util.*;

@Service
@Slf4j
public class ReadFromFileServiceImpl implements ReadFromFileService {

    @Override
    public List<String> getLines(String fileName) {
        try {
            String nonNullableFileName = Optional.ofNullable(fileName)
                    .orElseThrow(() -> new IllegalArgumentException("File name is null"));
            URL urlToResourceFile = getClass().getClassLoader().getResource(nonNullableFileName);
            if ((Objects.isNull(urlToResourceFile)) || (nonNullableFileName.isBlank())) {
                throw new IllegalArgumentException(String.format("File with name='%s' doesn't exists", fileName));
            }
            return new ArrayList<>(Files.readAllLines(Paths.get(urlToResourceFile.toURI())));
        } catch (IOException | URISyntaxException e) {
            log.error("Occurred error - {}", e.getMessage());
            return List.of();
        }
    }
}
