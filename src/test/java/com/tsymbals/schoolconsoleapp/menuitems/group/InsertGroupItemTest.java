package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InsertGroupItemTest {

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private GroupDtoMapper groupDtoMapper;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private InsertGroupItem insertGroupItem;

    private GroupDto groupDto;

    @BeforeEach
    void initialSetup() {
        groupService = new GroupServiceImpl(groupRepositoryMock, groupDtoMapper);
        insertGroupItem = new InsertGroupItem(groupService, inputValidationServiceMock);
        insertGroupItem.setInputValidationService(inputValidationServiceMock);

        groupDto = new GroupDto("AA-11");
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldInsertGroup_whenGroupNameIsNullOrEmpty(String name) {
        String validName = "AA-11";
        groupDto.setName(validName);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);

        assertDoesNotThrow(() -> groupService.saveGroup(groupDto));
        assertDoesNotThrow(() -> insertGroupItem.doAction());
    }
}
