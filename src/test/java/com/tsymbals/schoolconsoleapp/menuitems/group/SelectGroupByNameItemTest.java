package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.entity.Group;
import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectGroupByNameItemTest {

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private GroupDtoMapper groupDtoMapper;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private SelectGroupByNameItem selectGroupByNameItem;

    private List<Group> groups;
    private List<GroupDto> groupsDto;
    private GroupDto expectedGroupDto;

    @BeforeEach
    void initialSetup() {
        groupDtoMapper = new GroupDtoMapper();
        groupService = new GroupServiceImpl(groupRepositoryMock, groupDtoMapper);
        selectGroupByNameItem = new SelectGroupByNameItem(groupService, inputValidationServiceMock);
        selectGroupByNameItem.setInputValidationService(inputValidationServiceMock);

        groups = Collections.singletonList(new Group(1L, "AA-11"));
        groupsDto = Collections.singletonList(new GroupDto(1L, "AA-11", new HashSet<>()));
        expectedGroupDto = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldSelectGroupByName_whenGroupNameIsNullOrEmpty(String name) {
        String validName = "AA-11";
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(validName))
                .findFirst();
        Optional<GroupDto> optionalGroupDto = groupsDto.stream()
                .filter(groupDto -> groupDto.getName().equalsIgnoreCase(validName))
                .findFirst();
        optionalGroupDto.ifPresent(groupDto -> expectedGroupDto = groupDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(groupRepositoryMock.findByName(validName).map(groupDtoMapper)).thenReturn(optionalGroupDto);
        when(groupRepositoryMock.findByName(validName)).thenReturn(optionalGroup);

        assertNotNull(expectedGroupDto);
        assertDoesNotThrow(() -> groupService.getGroup(validName));
        assertDoesNotThrow(() -> selectGroupByNameItem.doAction());
        assertEquals(expectedGroupDto, selectGroupByNameItem.getGroupDto());
    }

    @Test
    void doAction_shouldCatchGroupNotFoundExceptionAndDisplayErrorMessage_whenGroupNameDoesNotExists() {
        String name = "BB-22";
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(name))
                .findFirst();
        Optional<GroupDto> optionalGroupDto = groupsDto.stream()
                .filter(groupDto -> groupDto.getName().equalsIgnoreCase(name))
                .findFirst();
        optionalGroupDto.ifPresent(groupDto -> expectedGroupDto = groupDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(groupRepositoryMock.findByName(name).map(groupDtoMapper)).thenReturn(optionalGroupDto);
        when(groupRepositoryMock.findByName(name)).thenReturn(optionalGroup);

        assertNull(expectedGroupDto);
        assertThrows(GroupNotFoundException.class, () -> groupService.getGroup(name));
        assertDoesNotThrow(() -> selectGroupByNameItem.doAction());
        assertEquals(expectedGroupDto, selectGroupByNameItem.getGroupDto());
    }
}
