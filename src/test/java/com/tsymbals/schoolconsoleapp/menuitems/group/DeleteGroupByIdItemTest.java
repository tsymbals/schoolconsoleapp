package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Group;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DeleteGroupByIdItemTest {

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private GroupDtoMapper groupDtoMapper;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private DeleteGroupByIdItem deleteGroupByIdItem;

    private List<Group> groups;
    private Group expectedGroup;

    @BeforeEach
    void initialSetup() {
        groupDtoMapper = new GroupDtoMapper();
        groupService = new GroupServiceImpl(groupRepositoryMock, groupDtoMapper);
        deleteGroupByIdItem = new DeleteGroupByIdItem(groupService, inputValidationServiceMock);
        deleteGroupByIdItem.setInputValidationService(inputValidationServiceMock);

        groups = Collections.singletonList(new Group(1L, "AA-11"));
        expectedGroup = null;
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldDeleteGroupById_whenGroupIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getId().equals(validId))
                .findFirst();
        optionalGroup.ifPresent(group -> expectedGroup = group);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(groupRepositoryMock.findById(validId)).thenReturn(optionalGroup);

        assertNotNull(expectedGroup);
        assertDoesNotThrow(() -> groupService.deleteGroup(validId));
        assertDoesNotThrow(() -> deleteGroupByIdItem.doAction());
    }

    @Test
    void doAction_shouldCatchGroupNotFoundExceptionAndDisplayErrorMessage_whenGroupIdDoesNotExists() {
        Long id = 2L;
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getId().equals(id))
                .findFirst();
        optionalGroup.ifPresent(group -> expectedGroup = group);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(groupRepositoryMock.findById(id)).thenReturn(optionalGroup);

        assertNull(expectedGroup);
        assertThrows(GroupNotFoundException.class, () -> groupService.deleteGroup(id));
        assertDoesNotThrow(() -> deleteGroupByIdItem.doAction());
    }
}
