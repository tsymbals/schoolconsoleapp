package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.entity.Group;
import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.exception.GroupNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.group.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectGroupByIdItemTest {

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private GroupDtoMapper groupDtoMapper;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private SelectGroupByIdItem selectGroupByIdItem;

    private List<Group> groups;
    private List<GroupDto> groupsDto;
    private GroupDto expectedGroupDto;

    @BeforeEach
    void initialSetup() {
        groupDtoMapper = new GroupDtoMapper();
        groupService = new GroupServiceImpl(groupRepositoryMock, groupDtoMapper);
        selectGroupByIdItem = new SelectGroupByIdItem(groupService, inputValidationServiceMock);
        selectGroupByIdItem.setInputValidationService(inputValidationServiceMock);

        groups = Collections.singletonList(new Group(1L, "AA-11"));
        groupsDto = Collections.singletonList(new GroupDto(1L, "AA-11", new HashSet<>()));
        expectedGroupDto = null;
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldSelectGroupById_whenGroupIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getId().equals(validId))
                .findFirst();
        Optional<GroupDto> optionalGroupDto = groupsDto.stream()
                .filter(groupDto -> groupDto.getId().equals(validId))
                .findFirst();
        optionalGroupDto.ifPresent(groupDto -> expectedGroupDto = groupDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(groupRepositoryMock.findById(validId).map(groupDtoMapper)).thenReturn(optionalGroupDto);
        when(groupRepositoryMock.findById(validId)).thenReturn(optionalGroup);

        assertNotNull(expectedGroupDto);
        assertDoesNotThrow(() -> groupService.getGroup(validId));
        assertDoesNotThrow(() -> selectGroupByIdItem.doAction());
        assertEquals(expectedGroupDto, selectGroupByIdItem.getGroupDto());
    }

    @Test
    void doAction_shouldCatchGroupNotFoundExceptionAndDisplayErrorMessage_whenGroupIdDoesNotExists() {
        Long id = 2L;
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getId().equals(id))
                .findFirst();
        Optional<GroupDto> optionalGroupDto = groupsDto.stream()
                .filter(groupDto -> groupDto.getId().equals(id))
                .findFirst();
        optionalGroupDto.ifPresent(groupDto -> expectedGroupDto = groupDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(groupRepositoryMock.findById(id).map(groupDtoMapper)).thenReturn(optionalGroupDto);
        when(groupRepositoryMock.findById(id)).thenReturn(optionalGroup);

        assertNull(expectedGroupDto);
        assertThrows(GroupNotFoundException.class, () -> groupService.getGroup(id));
        assertDoesNotThrow(() -> selectGroupByIdItem.doAction());
        assertEquals(expectedGroupDto, selectGroupByIdItem.getGroupDto());
    }
}
