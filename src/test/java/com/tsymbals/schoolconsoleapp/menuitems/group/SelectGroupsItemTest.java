package com.tsymbals.schoolconsoleapp.menuitems.group;

import com.tsymbals.schoolconsoleapp.dto.GroupDto;
import com.tsymbals.schoolconsoleapp.entity.Group;
import com.tsymbals.schoolconsoleapp.repository.GroupRepository;
import com.tsymbals.schoolconsoleapp.service.group.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectGroupsItemTest {

    @Mock
    private GroupRepository groupRepositoryMock;

    @InjectMocks
    private GroupDtoMapper groupDtoMapper;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private SelectGroupsItem selectGroupsItem;

    private List<Group> groups;
    private List<GroupDto> groupsDto;

    @BeforeEach
    void initialSetup() {
        groupDtoMapper = new GroupDtoMapper();
        groupService = new GroupServiceImpl(groupRepositoryMock, groupDtoMapper);
        selectGroupsItem = new SelectGroupsItem(groupService);

        groups = null;
        groupsDto = null;
    }

    @Test
    void doAction_shouldSelectGroups_whenGroupsExists() {
        groups = Collections.singletonList(new Group(1L, "AA-11"));
        groupsDto = Collections.singletonList(new GroupDto(1L, "AA-11", new HashSet<>()));

        when(groupRepositoryMock.findAll(Sort.by("id")).stream().map(groupDtoMapper).toList()).thenReturn(groupsDto);
        when(groupRepositoryMock.findAll(Sort.by("id"))).thenReturn(groups);

        assertFalse(groupsDto.isEmpty());
        assertDoesNotThrow(() -> groupService.getGroups());
        assertDoesNotThrow(() -> selectGroupsItem.doAction());
        assertTrue(groupsDto.containsAll(selectGroupsItem.getGroupsDto()));
    }

    @Test
    void doAction_shouldDisplayDoesNotExistsInfoMessage_whenGroupsDoesNotExists() {
        groups = Collections.emptyList();
        groupsDto = Collections.emptyList();

        when(groupRepositoryMock.findAll(Sort.by("id")).stream().map(groupDtoMapper).toList()).thenReturn(groupsDto);
        when(groupRepositoryMock.findAll(Sort.by("id"))).thenReturn(groups);

        assertTrue(groupsDto.isEmpty());
        assertDoesNotThrow(() -> groupService.getGroups());
        assertDoesNotThrow(() -> selectGroupsItem.doAction());
        assertEquals(groupsDto.size(), selectGroupsItem.getGroupsDto().size());
    }
}
