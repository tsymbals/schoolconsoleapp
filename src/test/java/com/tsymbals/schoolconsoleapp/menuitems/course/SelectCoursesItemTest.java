package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectCoursesItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private SelectCoursesItem selectCoursesItem;

    private List<Course> courses;
    private List<CourseDto> coursesDto;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        selectCoursesItem = new SelectCoursesItem(courseService);

        courses = null;
        coursesDto = null;
    }

    @Test
    void doAction_shouldSelectCourses_whenCoursesExists() {
        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        coursesDto = Collections.singletonList(new CourseDto(1L, "Name", "Description", new HashSet<>()));

        when(courseRepositoryMock.findAll(Sort.by("id")).stream().map(courseDtoMapper).toList()).thenReturn(coursesDto);
        when(courseRepositoryMock.findAll(Sort.by("id"))).thenReturn(courses);

        assertFalse(coursesDto.isEmpty());
        assertDoesNotThrow(() -> courseService.getCourses());
        assertDoesNotThrow(() -> selectCoursesItem.doAction());
        assertTrue(coursesDto.containsAll(selectCoursesItem.getCoursesDto()));
    }

    @Test
    void doAction_shouldDisplayDoesNotExistsInfoMessage_whenCoursesDoesNotExists() {
        courses = Collections.emptyList();
        coursesDto = Collections.emptyList();

        when(courseRepositoryMock.findAll(Sort.by("id")).stream().map(courseDtoMapper).toList()).thenReturn(coursesDto);
        when(courseRepositoryMock.findAll(Sort.by("id"))).thenReturn(courses);

        assertTrue(coursesDto.isEmpty());
        assertDoesNotThrow(() -> courseService.getCourses());
        assertDoesNotThrow(() -> selectCoursesItem.doAction());
        assertEquals(coursesDto.size(), selectCoursesItem.getCoursesDto().size());
    }
}
