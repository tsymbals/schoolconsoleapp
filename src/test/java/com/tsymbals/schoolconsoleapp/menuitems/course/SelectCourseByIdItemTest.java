package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectCourseByIdItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private SelectCourseByIdItem selectCourseByIdItem;

    private List<Course> courses;
    private List<CourseDto> coursesDto;
    private CourseDto expectedCourseDto;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        selectCourseByIdItem = new SelectCourseByIdItem(courseService, inputValidationServiceMock);
        selectCourseByIdItem.setInputValidationService(inputValidationServiceMock);

        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        coursesDto = Collections.singletonList(new CourseDto(1L, "Name", "Description", new HashSet<>()));
        expectedCourseDto = null;
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldSelectCourseById_whenCourseIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(validId))
                .findFirst();
        Optional<CourseDto> optionalCourseDto = coursesDto.stream()
                .filter(courseDto -> courseDto.getId().equals(validId))
                .findFirst();
        optionalCourseDto.ifPresent(courseDto -> expectedCourseDto = courseDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(courseRepositoryMock.findById(validId).map(courseDtoMapper)).thenReturn(optionalCourseDto);
        when(courseRepositoryMock.findById(validId)).thenReturn(optionalCourse);

        assertNotNull(expectedCourseDto);
        assertDoesNotThrow(() -> courseService.getCourse(validId));
        assertDoesNotThrow(() -> selectCourseByIdItem.doAction());
        assertEquals(expectedCourseDto, selectCourseByIdItem.getCourseDto());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundExceptionAndDisplayErrorMessage_whenCourseIdDoesNotExists() {
        Long id = 2L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(id))
                .findFirst();
        Optional<CourseDto> optionalCourseDto = coursesDto.stream()
                .filter(courseDto -> courseDto.getId().equals(id))
                .findFirst();
        optionalCourseDto.ifPresent(courseDto -> expectedCourseDto = courseDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(courseRepositoryMock.findById(id).map(courseDtoMapper)).thenReturn(optionalCourseDto);
        when(courseRepositoryMock.findById(id)).thenReturn(optionalCourse);

        assertNull(expectedCourseDto);
        assertThrows(CourseNotFoundException.class, () -> courseService.getCourse(id));
        assertDoesNotThrow(() -> selectCourseByIdItem.doAction());
        assertEquals(expectedCourseDto, selectCourseByIdItem.getCourseDto());
    }
}
