package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DeleteCourseByNameItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private DeleteCourseByNameItem deleteCourseByNameItem;

    private List<Course> courses;
    private Course expectedCourse;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        deleteCourseByNameItem = new DeleteCourseByNameItem(courseService, inputValidationServiceMock);
        deleteCourseByNameItem.setInputValidationService(inputValidationServiceMock);

        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        expectedCourse = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldDeleteCourseByName_whenCourseNameIsNullOrEmpty(String name) {
        String validName = "Name";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(validName))
                .findFirst();
        optionalCourse.ifPresent(course -> expectedCourse = course);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName);
        when(courseRepositoryMock.findByName(validName)).thenReturn(optionalCourse);

        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> courseService.deleteCourse(validName));
        assertDoesNotThrow(() -> deleteCourseByNameItem.doAction());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundExceptionAndDisplayErrorMessage_whenCourseNameDoesNotExists() {
        String name = "Course name";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(name))
                .findFirst();
        optionalCourse.ifPresent(course -> expectedCourse = course);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(courseRepositoryMock.findByName(name)).thenReturn(optionalCourse);

        assertNull(expectedCourse);
        assertThrows(CourseNotFoundException.class, () -> courseService.deleteCourse(name));
        assertDoesNotThrow(() -> deleteCourseByNameItem.doAction());
    }
}
