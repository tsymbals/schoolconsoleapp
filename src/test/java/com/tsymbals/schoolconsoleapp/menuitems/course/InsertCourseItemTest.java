package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InsertCourseItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private InsertCourseItem insertCourseItem;

    private String name;
    private String description;
    private CourseDto courseDto;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        insertCourseItem = new InsertCourseItem(courseService, inputValidationServiceMock);
        insertCourseItem.setInputValidationService(inputValidationServiceMock);

        name = "Name";
        description = "Description";
        courseDto = new CourseDto(name, description);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldInsertCourse_whenCourseNameIsNullOrEmpty(String name) {
        String validName = "Name";
        courseDto.setName(validName);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name).thenReturn(description);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName).thenReturn(description);

        assertDoesNotThrow(() -> courseService.saveCourse(courseDto));
        assertDoesNotThrow(() -> insertCourseItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldInsertCourse_whenCourseDescriptionIsNullOrEmpty(String description) {
        String validDescription = "Description";
        courseDto.setDescription(validDescription);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name).thenReturn(description);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name).thenReturn(validDescription);

        assertDoesNotThrow(() -> courseService.saveCourse(courseDto));
        assertDoesNotThrow(() -> insertCourseItem.doAction());
    }
}
