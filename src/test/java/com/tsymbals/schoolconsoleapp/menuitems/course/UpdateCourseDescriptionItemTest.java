package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UpdateCourseDescriptionItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private UpdateCourseDescriptionItem updateCourseDescriptionItem;

    private List<Course> courses;
    private Course expectedCourse;
    private String description;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        updateCourseDescriptionItem = new UpdateCourseDescriptionItem(courseService, inputValidationServiceMock);
        updateCourseDescriptionItem.setInputValidationService(inputValidationServiceMock);

        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        expectedCourse = null;
        description = "New description";
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldUpdateCourseDescription_whenCourseIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(validId))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setDescription(description);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(description);
        when(courseRepositoryMock.findById(validId)).thenReturn(optionalCourse);

        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> courseService.updateCourseDescription(validId, description));
        assertDoesNotThrow(() -> updateCourseDescriptionItem.doAction());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundExceptionAndDisplayErrorMessage_whenCourseIdDoesNotExists() {
        Long id = 2L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(id))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setDescription(description);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(description);
        when(courseRepositoryMock.findById(id)).thenReturn(optionalCourse);

        assertNull(expectedCourse);
        assertThrows(CourseNotFoundException.class, () -> courseService.updateCourseDescription(id, description));
        assertDoesNotThrow(() -> updateCourseDescriptionItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateCourseDescription_whenCourseDescriptionIsNullOrEmpty(String description) {
        Long id = 1L;
        String validDescription = "New description";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(id))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setDescription(validDescription);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(description);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validDescription);
        when(courseRepositoryMock.findById(id)).thenReturn(optionalCourse);

        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> courseService.updateCourseDescription(id, validDescription));
        assertDoesNotThrow(() -> updateCourseDescriptionItem.doAction());
    }
}
