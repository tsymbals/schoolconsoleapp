package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UpdateCourseNameItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private UpdateCourseNameItem updateCourseNameItem;

    private List<Course> courses;
    private Course expectedCourse;
    private String name;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        updateCourseNameItem = new UpdateCourseNameItem(courseService, inputValidationServiceMock);
        updateCourseNameItem.setInputValidationService(inputValidationServiceMock);

        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        expectedCourse = null;
        name = "New name";
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldUpdateCourseName_whenCourseIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(validId))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setName(name);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(courseRepositoryMock.findById(validId)).thenReturn(optionalCourse);

        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> courseService.updateCourseName(validId, name));
        assertDoesNotThrow(() -> updateCourseNameItem.doAction());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundExceptionAndDisplayErrorMessage_whenCourseIdDoesNotExists() {
        Long id = 2L;
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(id))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setName(name);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(courseRepositoryMock.findById(id)).thenReturn(optionalCourse);

        assertNull(expectedCourse);
        assertThrows(CourseNotFoundException.class, () -> courseService.updateCourseName(id, name));
        assertDoesNotThrow(() -> updateCourseNameItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateCourseName_whenCourseNameIsNullOrEmpty(String name) {
        Long id = 1L;
        String validName = "New name";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getId().equals(id))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedCourse.setName(validName);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName);
        when(courseRepositoryMock.findById(id)).thenReturn(optionalCourse);

        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> courseService.updateCourseName(id, validName));
        assertDoesNotThrow(() -> updateCourseNameItem.doAction());
    }
}
