package com.tsymbals.schoolconsoleapp.menuitems.course;

import com.tsymbals.schoolconsoleapp.dto.CourseDto;
import com.tsymbals.schoolconsoleapp.entity.Course;
import com.tsymbals.schoolconsoleapp.repository.CourseRepository;
import com.tsymbals.schoolconsoleapp.exception.CourseNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.course.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectCourseByNameItemTest {

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private CourseDtoMapper courseDtoMapper;

    @InjectMocks
    private CourseServiceImpl courseService;

    @InjectMocks
    private SelectCourseByNameItem selectCourseByNameItem;

    private List<Course> courses;
    private List<CourseDto> coursesDto;
    private CourseDto expectedCourseDto;

    @BeforeEach
    void initialSetup() {
        courseDtoMapper = new CourseDtoMapper();
        courseService = new CourseServiceImpl(courseRepositoryMock, courseDtoMapper);
        selectCourseByNameItem = new SelectCourseByNameItem(courseService, inputValidationServiceMock);
        selectCourseByNameItem.setInputValidationService(inputValidationServiceMock);

        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        coursesDto = Collections.singletonList(new CourseDto(1L, "Name", "Description", new HashSet<>()));
        expectedCourseDto = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldSelectCourseByName_whenCourseNameIsNullOrEmpty(String name) {
        String validName = "Name";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(validName))
                .findFirst();
        Optional<CourseDto> optionalCourseDto = coursesDto.stream()
                .filter(courseDto -> courseDto.getName().equalsIgnoreCase(validName))
                .findFirst();
        optionalCourseDto.ifPresent(courseDto -> expectedCourseDto = courseDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validName);
        when(courseRepositoryMock.findByName(validName).map(courseDtoMapper)).thenReturn(optionalCourseDto);
        when(courseRepositoryMock.findByName(validName)).thenReturn(optionalCourse);

        assertNotNull(expectedCourseDto);
        assertDoesNotThrow(() -> courseService.getCourse(validName));
        assertDoesNotThrow(() -> selectCourseByNameItem.doAction());
        assertEquals(expectedCourseDto, selectCourseByNameItem.getCourseDto());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundExceptionAndDisplayErrorMessage_whenCourseNameDoesNotExists() {
        String name = "Course name";
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(name))
                .findFirst();
        Optional<CourseDto> optionalCourseDto = coursesDto.stream()
                .filter(courseDto -> courseDto.getName().equalsIgnoreCase(name))
                .findFirst();
        optionalCourseDto.ifPresent(courseDto -> expectedCourseDto = courseDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(name);
        when(courseRepositoryMock.findByName(name).map(courseDtoMapper)).thenReturn(optionalCourseDto);
        when(courseRepositoryMock.findByName(name)).thenReturn(optionalCourse);

        assertNull(expectedCourseDto);
        assertThrows(CourseNotFoundException.class, () -> courseService.getCourse(name));
        assertDoesNotThrow(() -> selectCourseByNameItem.doAction());
        assertEquals(expectedCourseDto, selectCourseByNameItem.getCourseDto());
    }
}
