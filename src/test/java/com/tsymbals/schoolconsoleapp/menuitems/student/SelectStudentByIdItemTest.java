package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.entity.Student;
import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectStudentByIdItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private SelectStudentByIdItem selectStudentByIdItem;

    private List<Student> students;
    private List<StudentDto> studentsDto;
    private StudentDto expectedStudentDto;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        selectStudentByIdItem = new SelectStudentByIdItem(studentService, inputValidationServiceMock);
        selectStudentByIdItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        studentsDto = Collections.singletonList(new StudentDto(1L, null, "First", "Last", new HashSet<>()));
        expectedStudentDto = null;
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldSelectStudentById_whenStudentIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getId().equals(validId))
                .findFirst();
        Optional<StudentDto> optionalStudentDto = studentsDto.stream()
                .filter(studentDto -> studentDto.getId().equals(validId))
                .findFirst();
        optionalStudentDto.ifPresent(studentDto -> expectedStudentDto = studentDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(studentRepositoryMock.findById(validId).map(studentDtoMapper)).thenReturn(optionalStudentDto);
        when(studentRepositoryMock.findById(validId)).thenReturn(optionalStudent);

        assertNotNull(expectedStudentDto);
        assertDoesNotThrow(() -> studentService.getStudent(validId));
        assertDoesNotThrow(() -> selectStudentByIdItem.doAction());
        assertEquals(expectedStudentDto, selectStudentByIdItem.getStudentDto());
    }

    @Test
    void doAction_shouldCatchStudentNotFoundExceptionAndDisplayErrorMessage_whenStudentIdDoesNotExists() {
        Long id = 2L;
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getId().equals(id))
                .findFirst();
        Optional<StudentDto> optionalStudentDto = studentsDto.stream()
                .filter(studentDto -> studentDto.getId().equals(id))
                .findFirst();
        optionalStudentDto.ifPresent(studentDto -> expectedStudentDto = studentDto);

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(studentRepositoryMock.findById(id).map(studentDtoMapper)).thenReturn(optionalStudentDto);
        when(studentRepositoryMock.findById(id)).thenReturn(optionalStudent);

        assertNull(expectedStudentDto);
        assertThrows(StudentNotFoundException.class, () -> studentService.getStudent(id));
        assertDoesNotThrow(() -> selectStudentByIdItem.doAction());
        assertEquals(expectedStudentDto, selectStudentByIdItem.getStudentDto());
    }
}
