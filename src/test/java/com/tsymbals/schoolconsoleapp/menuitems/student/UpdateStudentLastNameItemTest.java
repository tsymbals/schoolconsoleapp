package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Student;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UpdateStudentLastNameItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private UpdateStudentLastNameItem updateStudentLastNameItem;

    private List<Student> students;
    private Student expectedStudent;
    private String lastName;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        updateStudentLastNameItem = new UpdateStudentLastNameItem(studentService, inputValidationServiceMock);
        updateStudentLastNameItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        expectedStudent = null;
        lastName = "Second";
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, 0})
    void doAction_shouldUpdateStudentLastName_whenStudentIdIsNegativeOrZero(Long id) {
        Long validId = 1L;
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getId().equals(validId))
                .findFirst();
        optionalStudent.ifPresent(student -> {
            expectedStudent = student;
            expectedStudent.setLastName(lastName);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(validId);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(lastName);
        when(studentRepositoryMock.findById(validId)).thenReturn(optionalStudent);

        assertNotNull(expectedStudent);
        assertDoesNotThrow(() -> studentService.updateStudentLastName(validId, lastName));
        assertDoesNotThrow(() -> updateStudentLastNameItem.doAction());
    }

    @Test
    void doAction_shouldCatchStudentNotFoundExceptionAndDisplayErrorMessage_whenStudentIdDoesNotExists() {
        Long id = 2L;
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getId().equals(id))
                .findFirst();
        optionalStudent.ifPresent(student -> {
            expectedStudent = student;
            expectedStudent.setLastName(lastName);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(lastName);
        when(studentRepositoryMock.findById(id)).thenReturn(optionalStudent);

        assertNull(expectedStudent);
        assertThrows(StudentNotFoundException.class, () -> studentService.updateStudentLastName(id, lastName));
        assertDoesNotThrow(() -> updateStudentLastNameItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentLastName_whenStudentLastNameIsNullOrEmpty(String lastName) {
        Long id = 1L;
        String validLastName = "Second";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getId().equals(id))
                .findFirst();
        optionalStudent.ifPresent(student -> {
            expectedStudent = student;
            expectedStudent.setLastName(validLastName);
        });

        when(inputValidationServiceMock.getValidIntegerNumber(any(Scanner.class), anyString(), anyString())).thenReturn(id);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validLastName);
        when(studentRepositoryMock.findById(id)).thenReturn(optionalStudent);

        assertNotNull(expectedStudent);
        assertDoesNotThrow(() -> studentService.updateStudentLastName(id, validLastName));
        assertDoesNotThrow(() -> updateStudentLastNameItem.doAction());
    }
}
