package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InsertStudentItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private InsertStudentItem insertStudentItem;

    private StudentDto studentDto;
    private String firstName;
    private String lastName;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        insertStudentItem = new InsertStudentItem(studentService, inputValidationServiceMock);
        insertStudentItem.setInputValidationService(inputValidationServiceMock);

        firstName = "First";
        lastName = "Last";
        studentDto = new StudentDto(firstName, lastName);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldInsertStudent_whenStudentFirstNameIsNullOrEmpty(String firstName) {
        String validFirstName = "First";
        studentDto.setFirstName(validFirstName);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validFirstName).thenReturn(lastName);

        assertDoesNotThrow(() -> studentService.saveStudent(studentDto));
        assertDoesNotThrow(() -> insertStudentItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldInsertStudent_whenStudentLastNameIsNullOrEmpty(String lastName) {
        String validLastName = "Last";
        studentDto.setLastName(validLastName);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(validLastName);

        assertDoesNotThrow(() -> studentService.saveStudent(studentDto));
        assertDoesNotThrow(() -> insertStudentItem.doAction());
    }
}
