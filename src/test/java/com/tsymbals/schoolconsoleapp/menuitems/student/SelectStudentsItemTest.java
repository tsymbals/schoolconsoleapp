package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.entity.Student;
import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectStudentsItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private SelectStudentsItem selectStudentsItem;

    private List<Student> students;
    private List<StudentDto> studentsDto;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        selectStudentsItem = new SelectStudentsItem(studentService);

        students = null;
        studentsDto = null;
    }

    @Test
    void doAction_shouldSelectStudents_whenStudentsExists() {
        students = Collections.singletonList(new Student(1L, "First", "Last"));
        studentsDto = Collections.singletonList(new StudentDto(1L, null, "First", "Last", new HashSet<>()));

        when(studentRepositoryMock.findAll(Sort.by("id")).stream().map(studentDtoMapper).toList()).thenReturn(studentsDto);
        when(studentRepositoryMock.findAll(Sort.by("id"))).thenReturn(students);

        assertFalse(studentsDto.isEmpty());
        assertDoesNotThrow(() -> studentService.getStudents());
        assertDoesNotThrow(() -> selectStudentsItem.doAction());
        assertTrue(studentsDto.containsAll(selectStudentsItem.getStudentsDto()));
    }

    @Test
    void doAction_shouldDisplayDoesNotExistsInfoMessage_whenStudentsDoesNotExists() {
        students = Collections.emptyList();
        studentsDto = Collections.emptyList();

        when(studentRepositoryMock.findAll(Sort.by("id")).stream().map(studentDtoMapper).toList()).thenReturn(studentsDto);
        when(studentRepositoryMock.findAll(Sort.by("id"))).thenReturn(students);

        assertTrue(studentsDto.isEmpty());
        assertDoesNotThrow(() -> studentService.getStudents());
        assertDoesNotThrow(() -> selectStudentsItem.doAction());
        assertEquals(studentsDto.size(), selectStudentsItem.getStudentsDto().size());
    }
}
