package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.entity.*;
import com.tsymbals.schoolconsoleapp.exception.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UpdateStudentByDeleteGroupItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private UpdateStudentByDeleteGroupItem updateStudentByDeleteGroupItem;

    private List<Student> students;
    private List<Group> groups;
    private String firstName;
    private String lastName;
    private String groupName;
    private Student expectedStudent;
    private Group expectedGroup;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        updateStudentByDeleteGroupItem = new UpdateStudentByDeleteGroupItem(studentService, inputValidationServiceMock);
        updateStudentByDeleteGroupItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        groups = Collections.singletonList(new Group(1L, "AA-11"));
        firstName = "First";
        lastName = "Last";
        groupName = "AA-11";
        expectedStudent = null;
        expectedGroup = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingGroup_whenStudentFirstNameIsNullOrEmpty(String firstName) {
        String validFirstName = "First";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(validFirstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(groupName))
                .findFirst();
        optionalGroup.ifPresent(group -> {
            expectedGroup = group;
            expectedStudent.setGroup(expectedGroup);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validFirstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(validFirstName, lastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(groupName)).thenReturn(optionalGroup);

        assertNotNull(expectedStudent);
        assertNotNull(expectedGroup);
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
        assertNull(expectedStudent.getGroup());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingGroup_whenStudentLastNameIsNullOrEmpty(String lastName) {
        String validLastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(validLastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(groupName))
                .findFirst();
        optionalGroup.ifPresent(group -> {
            expectedGroup = group;
            expectedStudent.setGroup(expectedGroup);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(validLastName).thenReturn(groupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, validLastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(groupName)).thenReturn(optionalGroup);

        assertNotNull(expectedStudent);
        assertNotNull(expectedGroup);
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
        assertNull(expectedStudent.getGroup());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingGroup_whenGroupNameIsNullOrEmpty(String groupName) {
        String validGroupName = "AA-11";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(validGroupName))
                .findFirst();
        optionalGroup.ifPresent(group -> {
            expectedGroup = group;
            expectedStudent.setGroup(expectedGroup);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(validGroupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(validGroupName)).thenReturn(optionalGroup);

        assertNotNull(expectedStudent);
        assertNotNull(expectedGroup);
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
        assertNull(expectedStudent.getGroup());
    }

    @Test
    void doAction_shouldCatchStudentNotFoundException_whenStudentNameDoesNotExists() {
        String firstName = "Second";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(groupName))
                .findFirst();
        optionalGroup.ifPresent(group -> expectedGroup = group);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(groupName)).thenReturn(optionalGroup);

        assertNull(expectedStudent);
        assertNotNull(expectedGroup);
        assertThrows(StudentNotFoundException.class, () -> studentService.updateStudentByDeletingGroup(firstName, lastName, groupName));
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
    }

    @Test
    void doAction_shouldCatchGroupNotFoundException_whenGroupNameDoesNotExists() {
        String groupName = "BB-22";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> {
            expectedStudent = student;
            expectedStudent.setGroup(new Group(2L, "CC-33"));
        });
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(groupName))
                .findFirst();
        optionalGroup.ifPresent(group -> expectedGroup = group);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(groupName)).thenReturn(optionalGroup);

        assertNotNull(expectedStudent);
        assertNull(expectedGroup);
        assertThrows(GroupNotFoundException.class, () -> studentService.updateStudentByDeletingGroup(firstName, lastName, groupName));
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
    }

    @Test
    void doAction_shouldCatchIllegalArgumentException_whenStudentGroupIsNull() {
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Group> optionalGroup = groups.stream()
                .filter(group -> group.getName().equalsIgnoreCase(groupName))
                .findFirst();
        optionalGroup.ifPresent(group -> expectedGroup = group);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(groupName);
        when(inputValidationServiceMock.isGroupNameMatchPattern(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(groupRepositoryMock.findByName(groupName)).thenReturn(optionalGroup);

        assertNotNull(expectedStudent);
        assertNotNull(expectedGroup);
        assertThrows(IllegalArgumentException.class, () -> studentService.updateStudentByDeletingGroup(firstName, lastName, groupName));
        assertDoesNotThrow(() -> updateStudentByDeleteGroupItem.doAction());
        assertNull(expectedStudent.getGroup());
    }
}
