package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.entity.*;
import com.tsymbals.schoolconsoleapp.exception.*;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UpdateStudentByDeleteCourseItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private UpdateStudentByDeleteCourseItem updateStudentByDeleteCourseItem;

    private List<Student> students;
    private List<Course> courses;
    private String firstName;
    private String lastName;
    private String courseName;
    private Student expectedStudent;
    private Course expectedCourse;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        updateStudentByDeleteCourseItem = new UpdateStudentByDeleteCourseItem(studentService, inputValidationServiceMock);
        updateStudentByDeleteCourseItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        courses = Collections.singletonList(new Course(1L, "Name", "Description"));
        firstName = "First";
        lastName = "Last";
        courseName = "Name";
        expectedStudent = null;
        expectedCourse = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingCourse_whenStudentFirstNameIsNullOrEmpty(String firstName) {
        String validFirstName = "First";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(validFirstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(courseName))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedStudent.getCourses().add(expectedCourse);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validFirstName).thenReturn(lastName).thenReturn(courseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(validFirstName, lastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(courseName)).thenReturn(optionalCourse);

        assertNotNull(expectedStudent);
        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
        assertFalse(expectedStudent.getCourses().contains(expectedCourse));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingCourse_whenStudentLastNameIsNullOrEmpty(String lastName) {
        String validLastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(validLastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(courseName))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedStudent.getCourses().add(expectedCourse);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(validLastName).thenReturn(courseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, validLastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(courseName)).thenReturn(optionalCourse);

        assertNotNull(expectedStudent);
        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
        assertFalse(expectedStudent.getCourses().contains(expectedCourse));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldUpdateStudentByDeletingCourse_whenCourseNameIsNullOrEmpty(String courseName) {
        String validCourseName = "Name";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(validCourseName))
                .findFirst();
        optionalCourse.ifPresent(course -> {
            expectedCourse = course;
            expectedStudent.getCourses().add(expectedCourse);
        });

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(validCourseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(validCourseName)).thenReturn(optionalCourse);

        assertNotNull(expectedStudent);
        assertNotNull(expectedCourse);
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
        assertFalse(expectedStudent.getCourses().contains(expectedCourse));
    }

    @Test
    void doAction_shouldCatchStudentNotFoundException_whenStudentNameDoesNotExists() {
        String firstName = "Second";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(courseName))
                .findFirst();
        optionalCourse.ifPresent(course -> expectedCourse = course);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(courseName)).thenReturn(optionalCourse);

        assertNull(expectedStudent);
        assertNotNull(expectedCourse);
        assertThrows(StudentNotFoundException.class, () -> studentService.updateStudentByDeletingCourse(firstName, lastName, courseName));
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
    }

    @Test
    void doAction_shouldCatchCourseNotFoundException_whenCourseNameDoesNotExists() {
        String courseName = "Not Name";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(courseName))
                .findFirst();
        optionalCourse.ifPresent(course -> expectedCourse = course);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(courseName)).thenReturn(optionalCourse);

        assertNotNull(expectedStudent);
        assertNull(expectedCourse);
        assertThrows(CourseNotFoundException.class, () -> studentService.updateStudentByDeletingCourse(firstName, lastName, courseName));
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
    }

    @Test
    void doAction_shouldCatchIllegalArgumentException_whenCourseDoesNotExists() {
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);
        Optional<Course> optionalCourse = courses.stream()
                .filter(course -> course.getName().equalsIgnoreCase(courseName))
                .findFirst();
        optionalCourse.ifPresent(course -> expectedCourse = course);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName).thenReturn(courseName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);
        when(courseRepositoryMock.findByName(courseName)).thenReturn(optionalCourse);

        assertNotNull(expectedStudent);
        assertNotNull(expectedCourse);
        assertThrows(IllegalArgumentException.class, () -> studentService.updateStudentByDeletingCourse(firstName, lastName, courseName));
        assertDoesNotThrow(() -> updateStudentByDeleteCourseItem.doAction());
        assertFalse(expectedStudent.getCourses().contains(expectedCourse));
    }
}
