package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.entity.Student;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DeleteStudentByNameItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private DeleteStudentByNameItem deleteStudentByNameItem;

    private List<Student> students;
    private Student expectedStudent;
    private String firstName;
    private String lastName;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        deleteStudentByNameItem = new DeleteStudentByNameItem(studentService, inputValidationServiceMock);
        deleteStudentByNameItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        expectedStudent = null;
        firstName = "First";
        lastName = "Last";
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldDeleteStudentByName_whenStudentFirstNameIsNullOrEmpty(String firstName) {
        String validFirstName = "First";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(validFirstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validFirstName).thenReturn(lastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(validFirstName, lastName)).thenReturn(optionalStudent);

        assertNotNull(expectedStudent);
        assertDoesNotThrow(() -> studentService.deleteStudent(validFirstName, lastName));
        assertDoesNotThrow(() -> deleteStudentByNameItem.doAction());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldDeleteStudentByName_whenStudentLastNameIsNullOrEmpty(String lastName) {
        String validLastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(validLastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(validLastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, validLastName)).thenReturn(optionalStudent);

        assertNotNull(expectedStudent);
        assertDoesNotThrow(() -> studentService.deleteStudent(firstName, validLastName));
        assertDoesNotThrow(() -> deleteStudentByNameItem.doAction());
    }

    @Test
    void doAction_shouldCatchStudentNotFoundExceptionAndDisplayErrorMessage_whenStudentNameDoesNotExists() {
        String firstName = "NotFirst";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudent.ifPresent(student -> expectedStudent = student);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);

        assertNull(expectedStudent);
        assertThrows(StudentNotFoundException.class, () -> studentService.deleteStudent(firstName, lastName));
        assertDoesNotThrow(() -> deleteStudentByNameItem.doAction());
    }
}
