package com.tsymbals.schoolconsoleapp.menuitems.student;

import com.tsymbals.schoolconsoleapp.dto.StudentDto;
import com.tsymbals.schoolconsoleapp.entity.Student;
import com.tsymbals.schoolconsoleapp.repository.*;
import com.tsymbals.schoolconsoleapp.exception.StudentNotFoundException;
import com.tsymbals.schoolconsoleapp.service.InputValidationService;
import com.tsymbals.schoolconsoleapp.service.student.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SelectStudentByNameItemTest {

    @Mock
    private StudentRepository studentRepositoryMock;

    @Mock
    private GroupRepository groupRepositoryMock;

    @Mock
    private CourseRepository courseRepositoryMock;

    @Mock
    private InputValidationService inputValidationServiceMock;

    @InjectMocks
    private StudentDtoMapper studentDtoMapper;

    @InjectMocks
    private StudentServiceImpl studentService;

    @InjectMocks
    private SelectStudentByNameItem selectStudentByNameItem;

    private List<Student> students;
    private List<StudentDto> studentsDto;
    private StudentDto expectedStudentDto;

    @BeforeEach
    void initialSetup() {
        studentDtoMapper = new StudentDtoMapper();
        studentService = new StudentServiceImpl(studentRepositoryMock, groupRepositoryMock, courseRepositoryMock, studentDtoMapper);
        selectStudentByNameItem = new SelectStudentByNameItem(studentService, inputValidationServiceMock);
        selectStudentByNameItem.setInputValidationService(inputValidationServiceMock);

        students = Collections.singletonList(new Student(1L, "First", "Last"));
        studentsDto = Collections.singletonList(new StudentDto(1L, null, "First", "Last", new HashSet<>()));
        expectedStudentDto = null;
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldSelectStudentByName_whenStudentFirstNameIsNullOrEmpty(String firstName) {
        String validFirstName = "First";
        String lastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(validFirstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        Optional<StudentDto> optionalStudentDto = studentsDto.stream()
                .filter(studentDto -> studentDto.getFirstName().equalsIgnoreCase(validFirstName) && studentDto.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudentDto.ifPresent(studentDto -> expectedStudentDto = studentDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(validFirstName).thenReturn(lastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(validFirstName, lastName).map(studentDtoMapper)).thenReturn(optionalStudentDto);
        when(studentRepositoryMock.findByFirstNameAndLastName(validFirstName, lastName)).thenReturn(optionalStudent);

        assertNotNull(expectedStudentDto);
        assertDoesNotThrow(() -> studentService.getStudent(validFirstName, lastName));
        assertDoesNotThrow(() -> selectStudentByNameItem.doAction());
        assertEquals(expectedStudentDto, selectStudentByNameItem.getStudentDto());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "   "})
    void doAction_shouldSelectStudentByName_whenStudentLastNameIsNullOrEmpty(String lastName) {
        String firstName = "First";
        String validLastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(validLastName))
                .findFirst();
        Optional<StudentDto> optionalStudentDto = studentsDto.stream()
                .filter(studentDto -> studentDto.getFirstName().equalsIgnoreCase(firstName) && studentDto.getLastName().equalsIgnoreCase(validLastName))
                .findFirst();
        optionalStudentDto.ifPresent(studentDto -> expectedStudentDto = studentDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(validLastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, validLastName).map(studentDtoMapper)).thenReturn(optionalStudentDto);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, validLastName)).thenReturn(optionalStudent);

        assertNotNull(expectedStudentDto);
        assertDoesNotThrow(() -> studentService.getStudent(firstName, validLastName));
        assertDoesNotThrow(() -> selectStudentByNameItem.doAction());
        assertEquals(expectedStudentDto, selectStudentByNameItem.getStudentDto());
    }

    @Test
    void doAction_shouldCatchStudentNotFoundExceptionAndDisplayErrorMessage_whenStudentNameDoesNotExists() {
        String firstName = "Second";
        String lastName = "Last";
        Optional<Student> optionalStudent = students.stream()
                .filter(student -> student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        Optional<StudentDto> optionalStudentDto = studentsDto.stream()
                .filter(studentDto -> studentDto.getFirstName().equalsIgnoreCase(firstName) && studentDto.getLastName().equalsIgnoreCase(lastName))
                .findFirst();
        optionalStudentDto.ifPresent(studentDto -> expectedStudentDto = studentDto);

        when(inputValidationServiceMock.getValidString(any(Scanner.class), anyString(), anyString())).thenReturn(firstName).thenReturn(lastName);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName).map(studentDtoMapper)).thenReturn(optionalStudentDto);
        when(studentRepositoryMock.findByFirstNameAndLastName(firstName, lastName)).thenReturn(optionalStudent);

        assertNull(expectedStudentDto);
        assertThrows(StudentNotFoundException.class, () -> studentService.getStudent(firstName, lastName));
        assertDoesNotThrow(() -> selectStudentByNameItem.doAction());
        assertEquals(expectedStudentDto, selectStudentByNameItem.getStudentDto());
    }
}
